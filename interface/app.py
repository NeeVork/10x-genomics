#!/usr/bin/env python3
"""Interface with tkinter"""

__author__ = "Naomi"
__version__ = "2019-12-05"
"""Interface with tkinter"""

__author__ = "Naomi"
__version__ = "2020-01-17"


import sys
import tkinter as tk

import constants as c
import styling
import pages.startpage as sp
import pages.page_one as p_one
import other_windows.help as helpwindow


class App(tk.Tk):
    """
    Main window of program
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.frames = dict()
        self.minsize(800, 400)

        logo = tk.PhotoImage(file=c.LOGO)
        self.iconphoto(True, logo)

        self.title(c.TITLE)

    def start(self):
        self.main_menu = self._make_main_menu()
        self.nav = self._make_side_nav()
        container = self._make_container()
        c.STATUS_BAR = StatusBar(self)
        c.STATUS_BAR.pack(side=tk.BOTTOM, fill=tk.X)

        self.nav.pack(side=tk.LEFT, fill="y")
        container.pack(side=tk.LEFT, fill="both", expand=True)

        for frame in (sp.StartPage, p_one.Page1):
            page = frame(container, self)
            self.frames[frame] = page
            page.grid(row=0, column=0, sticky="nsew")

        self.show_page(sp.StartPage)

    def _make_container(self):
        container = tk.Frame(self)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        return container

    def _make_side_nav(self):
        sidenav = styling.SideNav(self)
        # The lines below fill the sidenav.buttons list
        sidenav.add_button(text="Dimension reduction", side="top", selected=True)
        sidenav.add_button(text="Analyze clonotypes", side="top")
        sidenav.add_button(text="Help", side="bottom", command=helpwindow.HelpWindow.get_help)
        sidenav.add_button(text="Exit", side="bottom", command=self.destroy)

        # Add commands to the buttons
        sidenav.buttons[0].set_command(command=lambda: sidenav.navigate(sidenav.buttons[0], self, sp.StartPage))
        sidenav.buttons[1].set_command(command=lambda: sidenav.navigate(sidenav.buttons[1], self, p_one.Page1))

        return sidenav

    def _make_main_menu(self):
        menu = MainMenu(self, ("File", "file"), ("Edit", "edit"), ("View", "view"))
        file_dict = {
            "Open project": do_nothing,
            "separator1": "sep",
            "submenu1": ("recent", "Recent Files"),
            "separator2": "sep",
            "Exit": do_nothing}
        menu.fill_submenu("file", file_dict)
        return menu

    def show_page(self, page):
        frame = self.frames[page]
        frame.tkraise()


class MainMenu(tk.Menu):

    def __init__(self, master, *submenus):
        super().__init__(master)
        self.submenus = list()
        master.config(menu=self)
        for submenu in submenus:
            new_sub = tk.Menu(tearoff=0)
            setattr(self, submenu[1], new_sub)
            self.submenus.append(submenu[1])
            self.add_cascade(label=submenu[0], menu=new_sub)

    def fill_submenu(self, submenu, commands_dict):
        submenu = getattr(self, submenu)
        for key, value in commands_dict.items():
            if key.startswith("separator"):
                submenu.add_separator()
            elif key.startswith("submenu"):
                new_submenu = tk.Menu()
                setattr(self, value[0], new_submenu)
                self.submenus.append(value[0])
                submenu.add_cascade(label=value[1], menu=new_submenu)
            else:
                submenu.add_command(label=key, command=value)


class StatusBar(tk.Frame):

    def __init__(self, master):
        super().__init__(master, relief=tk.SUNKEN, bd=2)
        self.label = tk.Label(self)
        self.label.grid(sticky="nsw")
        self.show_msg("Welcome to CLAP tenX", time=5000)

    def show_msg(self, msg, time=None):
        self.label.config(text=msg)
        self.label.grid()
        if time:
            self.label.after(time, self.remove_msg)

    def remove_msg(self):
        self.label.grid_remove()


def do_nothing(event=None):
    print("Doing nothing")


def main():
    c.APP = App()
    c.APP.start()
    c.APP.mainloop()
    c.TEMP_DIR.cleanup()
    return 0


if __name__ == "__main__":
    sys.exit(main())
