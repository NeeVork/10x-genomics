#!/usr/bin/env python3
"""Interface with tkinter"""

__author__ = "Naomi"
__version__ = "2020-01-16"

import math
import tkinter as tk
import tkinter.ttk as ttk
from PIL import Image, ImageTk

import constants as c
import other_windows.save_file as save_file


class BorderedFrame(tk.Frame):

    def __init__(
            self, master,
            bd_top=0, bd_right=0, bd_bottom=0, bd_left=0,
            bd_color="black",
            inner_widget=tk.Frame,
            pack_options=dict(), **kwargs):
        super().__init__(master, bg=bd_color)

        self.inner = inner_widget(self, **kwargs)

        self.inner.pack(padx=(bd_left, bd_right), pady=(bd_top, bd_bottom), fill="both", **pack_options)


class Scrollable(tk.Frame):
    """
    Make a frame scrollable with scrollbar on the right.
    After adding or removing widgets to the scrollable frame,
    call the update() method to refresh the scrollable area.
    """

    def __init__(self, frame, width=16, **kwargs):

        scrollbar = tk.Scrollbar(frame)
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y, expand=False)

        self.canvas = tk.Canvas(frame, yscrollcommand=scrollbar.set, **kwargs)
        self.canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        scrollbar.config(command=self.canvas.yview)

        self.canvas.bind('<Configure>', self.__fill_canvas)

        # base class initialization
        tk.Frame.__init__(self, frame, **kwargs)

        # assign this obj (the inner frame) to the windows item of the canvas
        self.windows_item = self.canvas.create_window(0,0, window=self, anchor=tk.NW)


    def __fill_canvas(self, event):
        "Enlarge the windows item to the canvas width"

        canvas_width = event.width
        self.canvas.itemconfig(self.windows_item, width = canvas_width)

    def update(self):
        "Update the canvas and the scrollregion"

        self.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox(self.windows_item))


class WrappingLabel(tk.Label):
    """
    a type of Label that automatically adjusts the wrap to the size
    """
    def __init__(self, master=None, **kwargs):
        super().__init__(master, **kwargs)
        self.bind('<Configure>', lambda e: self.config(wraplength=self.winfo_width()))


class MinMaxFrame(tk.Frame):
    """
    tkinter frame to display plus and minus symbol to be able to minimize and maximize the side navigation
    """

    def __init__(self, master, config_dict, update=None, *targets, **kwargs):
        super().__init__(master, **kwargs)
        self.first_time = True
        self.update = update

        self.min_button = tk.Button(self, command=lambda: self.minimize(targets))
        self.max_button = tk.Button(self, command=lambda: self.maximize(targets))

        self.button_config(**config_dict)

        self.min_button.grid()

    def button_config(self, **kwargs):
        if self.first_time:
            self.min_button.config(text="-")
            self.max_button.config(text="+")
            self.first_time = False
        for button in (self.min_button, self.max_button):
            button.config(**kwargs)

    def minimize(self, targets):
        self.min_button.grid_remove()
        self.max_button.grid()
        for target in targets:
            target.grid_remove()
        if self.update:
            self.update.update()

    def maximize(self, targets):
        self.min_button.grid()
        self.max_button.grid_remove()
        for target in targets:
            target.grid()
        if self.update:
            self.update.update()

    def config_min(self, **kwargs):
        self.min_button.config(**kwargs)

    def config_max(self, **kwargs):
        self.max_button.config(**kwargs)

    def set_max_button_sign(self, text=None, image=None):
        if text:
            self.max_button.config(text=text)
        if image:
            self.max_image = tk.PhotoImage(file=image)
            self.max_button.config(image=self.max_image)

    def set_min_button_sign(self, text=None, image=None):
        if text:
            self.min_button.config(text=text)
        if image:
            self.min_image = tk.PhotoImage(file=image)
            self.min_button.config(image=self.min_image)


class SideNav(tk.Frame):
    """
    Menu displayed on the left hand side to navigate in the program
    """

    def __init__(self, master, text="Side nav"):
        super().__init__(master, bg=c.SIDE_BG_MAIN)
        #self.frames = dict()
        self.buttons = list()

        # Make widgets
        topbar = tk.Frame(self, bg=c.DARK_GRAY)

        label = tk.Label(topbar, text=text, bg=c.DARK_GRAY, anchor="sw", fg=c.SIDE_TEXT_HEADER)

        self.top_buttons = tk.Frame(self, bg=c.SIDE_BG_MAIN)
        self.bottom_buttons = tk.Frame(self, bg=c.SIDE_BG_MAIN)

        min_max_button = MinMaxFrame(topbar, c.MIN_MAX_BUTOTN_SETTINGS, None, self.top_buttons, self.bottom_buttons, label)
        min_max_button.config_max(bg=c.SIDE_BG_MAIN)

        # Place widgets in self
        topbar.grid(column=0, row=0, sticky="nsew")
        self.top_buttons.grid(column=0, row=1, sticky="nsew", padx=(20, 0), pady=(5, 0))
        self.bottom_buttons.grid(column=0, row=2, sticky="nsew", padx=(20, 0), pady=(5, 0))

        # Place widgets in topbar
        label.grid(column=0, row=0, sticky="nsew", padx=(10, 80), pady=(5, 5))
        min_max_button.grid(column=1, row=0, sticky="nsew")

    def add_button(self, text, selected=False, side="top", command=None):
        if side == "top":
            place_in = self.top_buttons
        elif side == "bottom":
            place_in = self.bottom_buttons

        button = SideNavButton(master=place_in, text=text, selected=selected, command=command)
        button.pack(fill="x", expand=True, pady=(0, 5))

        self.buttons.append(button)

    def navigate(self, button, root, destination):
        root.show_page(destination)
        for b in self.buttons:
            b.state(("!alternate",))
        button.state(("alternate",))


class SideNavButton(ttk.Button):

    def __init__(self, master, text, selected=False, command=None):
        # Make button style
        style = ttk.Style()
        style.theme_use('classic')
        style.configure("nav_button.TButton", **c.NAV_BUTTON_CONFIG)
        style.map("nav_button.TButton", **c.NAV_BUTTON_MAP)

        super().__init__(
            master=master,
            text=text,
            style="nav_button.TButton"
        )

        if command:
            self.set_command(command=command)

        if selected:
            self.state(("alternate",))

    def set_command(self, command):
        self.config(command=command)


class Page(tk.Frame):

    def __init__(self, master, name):
        super().__init__(master)

        # Scrollable frame that can hold all the rows (or widgets) of the page
        self.scrollable_frame = Scrollable(self, **c.SCROLLABLE_PAGE_SETTINGS)

        # Make scrollable window resize on the x axis with the window
        self.scrollable_frame.grid_columnconfigure(0, weight=1)
        # Set padding of scrollable frame
        self.scrollable_frame.config(pady=10, padx=10)

        # Make header for the page
        header = tk.Label(self.scrollable_frame, text=name, **c.PAGE_HEADER_SETTINGS)
        header.grid(row=0, column=0, pady=(0, 10), sticky="nsew")


class Row(BorderedFrame):

    def __init__(self, master, min_max=False, header_text=None):
        super().__init__(master=master, bd_top=0, bd_right=0, bd_bottom=1, bd_left=0, bd_color="black",
                         inner_widget=tk.Frame, pack_options=dict(), bg=c.PAGE_BG)
        self.master = master
        # Make frame resize when window changes size
        self.inner.columnconfigure(0, weight=1)

        # Devide layout in header and content
        self.header = tk.Frame(self.inner, bg="#2f5a63")
        self.content = tk.Frame(self.inner, bg=c.PAGE_BG)

        # Configure resize for header
        self.header.columnconfigure(1, weight=1)

        # Place header and content with grid
        self.header.grid(row=0, column=0, sticky="nsew")
        self.content.grid(row=1, column=0, sticky="nsew", pady=(10, 0))

        if min_max:
            self.add_min_max()

        if header_text:
            self.add_header_text(header_text)

    def add_header_text(self, text):
        label = tk.Label(self.header, text=text, **c.PAGE_ROW_HEADER_SETTINGS)
        label.grid(row=0, column=0, sticky="nsew", pady=(0, 5))

    def add_min_max(self):
        min_max = MinMaxFrame(self.header, c.ROW_MIN_MAX_SETTINGS, self.master, self.content)
        min_max.grid(row=0, column=1, sticky="ne")
        min_max.set_max_button_sign(image=c.ARROW_DOWN)
        min_max.set_min_button_sign(image=c.ARROW_UP)
        min_max.minimize([self.content])


class PageButton(ttk.Button):

    def __init__(self, master, text=None, image=None, command=None):
        style = ttk.Style()
        style.theme_use('classic')
        style.configure("page_button.TButton", **c.PAGE_BUTTON_CONFIG)
        style.map("page_button.TButton", **c.PAGE_BUTTON_MAP)

        super().__init__(
            master=master,
            style="page_button.TButton",
            command=command
        )

        if text and image:
            self.config(compound="left")

        if text:
            self.config(text=text)

        if image:
            self.config(image=image)


class OpenFileButton(PageButton):

    def __init__(self, master, command):
        self.dir_image = tk.PhotoImage(file=c.FOLDER)
        super().__init__(
            master=master,
            image=self.dir_image,
            command=command
        )


class PlotDisplay(BorderedFrame):
    """
    An object to display images (plots) that places up to 3 plots next to each other (if the width allows it)
    """

    def __init__(self, master, update, title=None, **kwargs):
        super().__init__(master = master,
                         bd_top=1, bd_right=1, bd_bottom=1, bd_left=1, bd_color="white",
                         inner_widget=tk.Frame, pack_options=dict(), bg=c.PAGE_BG)

        # Set some variables
        self.update = update
        self.plots = list()
        self.plot_paths = list()
        self.prev_screen_width = None
        self.current_screen_width = self.winfo_width()

        # Calculate widths needed for the screen that is being used
        screen_width = c.APP.winfo_screenwidth()
        available_width = screen_width - 250
        self.padding = math.floor(available_width * 0.01)
        self.image_width = math.floor((available_width - 2 * self.padding) / 3)

        # Width where there will be 2 plots instead of 3
        self.break_1 = 3 * self.image_width + 3 * self.padding

        # Width where there will be 1 plots instead of 2
        self.break_2 = 2 * self.image_width + 2 * self.padding

        # Self is binded to the functions self.screen_width_change, it will be activated when
        self.bind('<Configure>', self._screen_width_change)

        # Set first column to change size when window resizes
        self.inner.columnconfigure(0, weight=1)

        self.save_image = tk.PhotoImage(file=c.SAVE)
        self.save_button = PageButton(
            master=self.inner,
            text="Save image(s)",
            image=self.save_image,
            command=self.save_plots
        )

        if title:
            title = tk.Label(self.inner, text=title, **c.PLOTS_ROW_HEADER_SETTINGS)
            title.grid(row=0, column=0, columnspan=5)

    def add_plot(self, plot_path):
        """
        :param plot_path: path to an image file
        With the given path open the image with PIL.Image, convert to RGBA, resize image. With resized image make
        a Tk PhotoImage (with PIL.ImageTk.PhotoImage) and saves as an atribute of the class (otherwise will be
        garbage collected and won't show up later). The PhotoImage is put in a tk.Label and appended to the self.plots
        list. All the plots in the self.plots list will be placed with self.place_plots method.
        """
        self.plot_paths.append(plot_path)
        plot_name = "plot{}".format(len(self.plots))
        plot_img = Image.open(plot_path)
        plot_img = plot_img.convert("RGBA")
        plot_img = plot_img.resize((self.image_width, self.image_width), resample=Image.ANTIALIAS)

        setattr(self, plot_name, ImageTk.PhotoImage(plot_img))
        plot = tk.Label(self.inner, image=getattr(self, plot_name))
        self.plots.append(plot)

        if self.current_screen_width > self.break_1:
            self.inner.columnconfigure(2, weight=0)
            self.inner.columnconfigure(3, weight=0)
            self.inner.columnconfigure(4, weight=1)
            self._place_plots(3)
        elif self.current_screen_width > self.break_2:
            self.inner.columnconfigure(2, weight=0)
            self.inner.columnconfigure(3, weight=1)
            self.inner.columnconfigure(4, weight=0)
            self._place_plots(2)
        else:
            self.inner.columnconfigure(2, weight=1)
            self.inner.columnconfigure(3, weight=0)
            self.inner.columnconfigure(4, weight=0)
            self._place_plots(1)

    def _place_plots(self, plots_per_row):
        print(plots_per_row)
        self.save_button.grid_forget()
        for i, plot in enumerate(self.plots):
            plot.grid_forget()
            row = math.floor(i/plots_per_row) + 1
            column = (i % plots_per_row + 1)
            if column < plots_per_row:
                padx = (0, self.padding)
            else:
                padx = 0
            plot.grid(row=row, column=column, sticky="nsw", pady=self.padding, padx=padx)
        self.save_button.grid(row=row+1, column=0, columnspan=5, sticky="sw", padx=(10, 0), pady=(0, 10))
        self.update.update()

    def _screen_width_change(self, event):
        """
        :param event: The event that triggers this method
        Checks previous and current screenwidth, if it passes a threshold (self.break_1 or self.break_2), change
        columnconfigure and how many plots will be displayed next too each other.
        """
        if len(self.plots) > 1:
            self.prev_screen_width = self.current_screen_width
            self.current_screen_width = event.width

            if self.current_screen_width < self.break_2 <= self.prev_screen_width:
                self.inner.columnconfigure(2, weight=1)
                self.inner.columnconfigure(3, weight=0)
                self.inner.columnconfigure(4, weight=0)
                self._place_plots(1)
            elif self.current_screen_width < self.break_1 <= self.prev_screen_width:
                self.inner.columnconfigure(2, weight=0)
                self.inner.columnconfigure(3, weight=1)
                self.inner.columnconfigure(4, weight=0)
                self._place_plots(2)

            elif self.prev_screen_width < self.break_1 <= self.current_screen_width:
                self.inner.columnconfigure(2, weight=0)
                self.inner.columnconfigure(3, weight=0)
                self.inner.columnconfigure(4, weight=1)
                self._place_plots(3)
            elif self.prev_screen_width < self.break_2 <= self.current_screen_width:
                self.inner.columnconfigure(2, weight=0)
                self.inner.columnconfigure(3, weight=1)
                self.inner.columnconfigure(4, weight=0)
                self._place_plots(2)

    def delete_plots(self):
        for i, plot in enumerate(self.plots):
            plot.grid_forget()
            delattr(self, "plot{}".format(i))
        self.plots = list()
        self.plot_paths = list()

    def save_plots(self):
        save_file.SaveImages.open(tuple(self.plot_paths))
