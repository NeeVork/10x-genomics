#!usr/bin/env python3
"""Constants"""

__author__ = "Naomi"
__version__ = "2020-01-16"

import os
import platform
import tempfile
import tkinter as tk

#############################
# ######## General ######## #
#############################
# Title
TITLE = "CLAP tenX"

# Specific to user
OPERATING_SYS = platform.system()
WD = os.getcwd()
RSCRIPT_EXE = r'{}R\R-3.6.2\bin\Rscript.exe'.format(WD[0:-9])
TEMP_DIR = tempfile.TemporaryDirectory(dir=WD[0:-9])

# Will be made and used across multiple modules
APP = None
STATUS_BAR = None

############################
# ######## Images ######## #
############################

LOGO = r"images/cel_logo.png"
# LOGO_ICO = r"images/cel_logo.ico"

LOADING = r"images/pending-loop.gif"

ARROW_DOWN = r"images/down_arrow_icon.png"

ARROW_UP = r"images/up_arrow_icon.png"

FOLDER = r"images/folder_open.png"

SAVE = r"images/save.png"

# Plots
ELBOW_PLOT = r"{}/pca_elbow_plot.png".format(TEMP_DIR.name)

VIOLIN_PLOT_BFR = list()
for i in range(1, 4):
    VIOLIN_PLOT_BFR.append(r"{}/vln_before_filtering{}.png".format(TEMP_DIR.name, i))

VIOLIN_PLOT_AFT = list()
for i in range(1, 4):
    VIOLIN_PLOT_AFT.append(r"{}/vln_after_filtering{}.png".format(TEMP_DIR.name, i))

BARPLOT_CLONOTYPES = r"{}/freq_clonotypes.png".format(TEMP_DIR.name)

BARPLOT_CLONOTYPES_COMPARE = r"{}/freq_clonotypes_compare.png".format(TEMP_DIR.name)

TSNE_1 = r"{}/tsne_1.png".format(TEMP_DIR.name)

TSNE_HIGHLIGHTED = r"{}/tsne_highlight_contig.png".format(TEMP_DIR.name)

TSNE_TOP5_1 = r"{}/pbmc_tsne_top5.png".format(TEMP_DIR.name)
TSNE_TOP5_2 = r"{}/pbmc_tsne_top5_highlight.png".format(TEMP_DIR.name)
TSNE_TOP5_3 = r"{}/clonotype_tsne_top5.png".format(TEMP_DIR.name)
TSNE_TOP5_4 = r"{}/clonotype_tsne_top5_highlight.png".format(TEMP_DIR.name)
TSNE_TOP5_5 = r"{}/clonotype_top_5.png".format(TEMP_DIR.name)

############################
# ######## Colors ######## #
############################

# SIDENAV
SIDE_TEXT_HEADER = "#ffffff"
SIDE_TEXT_MAIN = "#ffffff"
SIDE_BUTTON_TEXT_HOVER = "#a5c8cf"
SIDE_BUTTON_PLUS_MIN = "#a5c8cf"

SIDE_BG_MAIN = "#4D585D"
SIDE_BG_HIGHLIGHT = "#282e30"
SIDE_BUTTON_BG_HOVER = "#282e30"
SIDE_BUTTON_BG_OPEN = "#487a85"

# PAGES
PAGE_HEADER_FG = "#000000"

PAGE_BG = "#487a85"

PAGE_FG = "#ffffff"

LIGHT_BLUE = "#a5c8cf"
MID_BLUE = "#487a85"

MID_GRAY = "#4D585D"
DARK_GRAY = "#282e30"


###########################
# ######## Fonts ######## #
###########################

LARGE_FONT = ("Verdana", 14)
MEDIUM_FONT = ("Verdana", 11)
SMALL_FONT = ("Verdana", 9)


#######################################
# ### tkinter/ttk widget Settings ### #
#######################################

MIN_MAX_BUTOTN_SETTINGS = {
    "bd": 0,
    "bg": SIDE_BG_HIGHLIGHT,
    "highlightthickness": 0,
    "font": LARGE_FONT,
    "fg": SIDE_BUTTON_PLUS_MIN
}

NAV_BUTTON_CONFIG = {
    "background": SIDE_BG_MAIN,
    "foreground": SIDE_TEXT_MAIN,
    "anchor": "w",
    "highlightthickness": 0,
    "relief": "flat"
}

NAV_BUTTON_MAP = {
    "background": [("active", SIDE_BUTTON_BG_HOVER), ("alternate", SIDE_BUTTON_BG_OPEN)],
    "foreground": [("active", SIDE_BUTTON_TEXT_HOVER), ("alternate", SIDE_TEXT_MAIN)]
}



SCROLLABLE_PAGE_SETTINGS = {
    "width": 16,
    "bd": 0,
    "relief": None,
    "highlightthickness": 0,
    "bg": PAGE_BG
}

PAGE_HEADER_SETTINGS = {
    "bg": PAGE_BG,
    "fg": PAGE_HEADER_FG,
    "font": LARGE_FONT
}

PAGE_ROW_HEADER_SETTINGS = {
    "bg": "#2f5a63",
    "fg": PAGE_FG,
    "font": MEDIUM_FONT
}

PAGE_LABEL_SETTINGS = {
    "bg": PAGE_BG,
    "fg": PAGE_FG,
    "font": SMALL_FONT,
    "anchor": tk.W,
    "justify": tk.LEFT
}

PAGE_INFO_LABEL_SETTINGS = {
    "bg": PAGE_BG,
    "fg": "#333333",
    "font": SMALL_FONT,
    "anchor": tk.W,
    "justify": tk.LEFT
}

PAGE_INFO_DISABLED = {
    "fg": "#a6a6a6"
}

PAGE_WARNING_SETTINGS = {
    "bg": PAGE_BG,
    "fg": "red",
    "font": SMALL_FONT,
    "anchor": tk.W,
    "justify": tk.LEFT
}

PLOTS_ROW_HEADER_SETTINGS = {
    "bg": PAGE_BG,
    "fg": PAGE_FG,
    "font": MEDIUM_FONT
}

ROW_MIN_MAX_SETTINGS = {
    "bd": 0,
    "bg": "#2f5a63",
    "highlightthickness": 0,
    "font": LARGE_FONT,
    "fg": PAGE_FG
}

PAGE_BUTTON_CONFIG = {
    "background": "#9acdd9",
    "highlightthickness": 2,
    "highlightcolor": "#355d66",
    "relief": "flat",
    "padding": 0,
    "compound": "left"
}

PAGE_BUTTON_MAP = {
    "background": [("disabled", "#b6ced4"), ("active", "#639ba8")],
    "foreground": [("disabled", "#7b8b8f"), ("active", "white")],
    "highlightcolor": [("pressed", "black"), ("active", "#9acdd9")], # #355d66
    "relief": [("pressed", "flat")]
}

RADIOBUTTON_CONFIG = {
    "bg": PAGE_BG
}

OPTION_MENU_CONFIG = {
    "background": "#9acdd9",
    "highlightthickness": 2,
    "highlightcolor": "#355d66",
    "relief": "flat",
    "compound": "left"
}

OPTION_MENU_MAP = {
    "background": [("disabled", "#b6ced4"), ("active", "#639ba8")],
    "foreground": [("disabled", "#7b8b8f"), ("active", "white")],
    "highlightcolor": [("pressed", "black"), ("active", "#9acdd9")], # #355d66
    "relief": [("pressed", "flat")]
}