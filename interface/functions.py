#!usr/bin/env python3
"""Startpage"""

__author__ = "Naomi"
__version__ = "2020-01-16"


import threading
import re
import subprocess

import constants as c


def thread_task(function, args=()):
    thread = threading.Thread(target=function, args=args)
    thread.daemon = True
    thread.start()


def is_int(test_case):
    try:
        test_case = int(test_case)
        return True
    except ValueError:
        if not test_case:
            return True
        return False


def is_float(test_case):
    try:
        test_case = float(test_case)
        return True
    except ValueError:
        if not test_case:
            return True
        return False


def shorten_dir_name(dir_name):
    if len(dir_name) > 50:
        # If directory path is too long make shorter path maximum length of 50 and doesn't start in the middle of a
        # name
        end_of_dir_name = dir_name[-50::]
        pattern = r"[/\\][^.]+"
        match = re.search(pattern, end_of_dir_name)
        # show_dir_name = match.group()
        if match:
            short_dir_name = "...{}".format(match.group())
        else:
            short_dir_name = "...{}".format(end_of_dir_name)
    else:
        # If a directory is selected and name is not too long display full path name
        short_dir_name = dir_name
    return short_dir_name


def run_r_script(script_path, *args):
    args = [str(x) for x in args]
    if c.OPERATING_SYS == "Windows":
        subprocess.call(
            ["cmd", "/c", c.RSCRIPT_EXE, script_path, *args])


def delete_text(event):
    if event.widget.default_text:
        event.widget.delete(0, 100)
        event.widget.default_text = False