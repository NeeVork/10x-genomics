#!usr/bin/env python3
"""
Pop up window to ask the user where and how to save the file
"""

__author__ = "Naomi"
__version__ = "2020-01-14"

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.filedialog as tkfile
import tkinter.messagebox as msgbox

from PIL import Image, ImageTk

import constants as c
import styling
import functions


class SaveImages(tk.Toplevel):

    active = dict()

    def __init__(self, plots):
        """

        :param plots: A list of paths to image files
        """
        super().__init__(c.APP, height=300, width=200, bg=c.PAGE_BG)

        # Set some attributes (some to be filled later)
        self.plot_paths = plots
        self.plots = list()
        self.dir_path = str()

        # Set protocol for when the window closes
        self.protocol('WM_DELETE_WINDOW', self._close_window_proc)

        # Set title and minimum size for the window
        self.title("Save image as")
        self.minsize(700, 500)

        # Make scrollable container to place all the widgets
        container = styling.Scrollable(self, bg=c.PAGE_BG)
        container.grid_columnconfigure(5, weight=1)

        # Function to check if input is integer
        validate_int = self.register(functions.is_int)

        # Make dropdown menu (ttk.OptionMenu) style
        style = ttk.Style()
        style.theme_use('classic')
        style.configure("select_extension.TMenubutton", **c.OPTION_MENU_CONFIG)
        style.map("select_extension.TMenubutton", **c.OPTION_MENU_MAP)

        for i, plot_path in enumerate(self.plot_paths):

            # Open image with plot_path, convert image to RGBA for antialias append plot to self.plots for later use
            plot_img = Image.open(plot_path)
            plot_img = plot_img.convert("RGBA")
            self.plots.append([plot_img, True])

            # Resize image to show small version
            plot_img = plot_img.resize((200, 200), resample=Image.ANTIALIAS)

            # Generate name for plot and store as attribute (to avoid it being garbage collected)
            plot_name = "plot{}".format(i)
            setattr(self, plot_name, ImageTk.PhotoImage(plot_img))

            # Put the image it in a label
            plot = tk.Label(master=container, image=getattr(self, plot_name), bg=c.PAGE_BG)

            # Make checkbox to indicate to save this specific plot
            setattr(self, "plot{}_bool".format(i), tk.BooleanVar())
            checkbox = tk.Checkbutton(
                master=container,
                variable=getattr(self, "plot{}_bool".format(i)),
                command=self._checkbox_action,
                bg=c.PAGE_BG
            )

            # Make a frame to display all the settings that can be adjusted
            save_settings = tk.Frame(master=container, bg=c.PAGE_BG)

            # Label + entry for name of the plot
            plot_name_label = tk.Label(
                master=save_settings,
                text="Name of file (without extension):",
                **c.PAGE_LABEL_SETTINGS
            )
            setattr(self, "plot{}_name_label".format(i), plot_name_label)
            setattr(self, "plot{}_name".format(i), tk.StringVar())
            plot_name_entry = tk.Entry(
                master=save_settings,
                textvariable=getattr(self, "plot{}_name".format(i)),
                disabledbackground=c.PAGE_BG
            )
            setattr(self, "plot{}_name_entry".format(i), plot_name_entry)

            # Label + entry for size of the plot (validates if entry is integer)
            plot_size_label = tk.Label(
                master=save_settings,
                text="Size (width and height will be set to this):",
                **c.PAGE_LABEL_SETTINGS
            )
            setattr(self, "plot{}_size_label".format(i), plot_size_label)
            setattr(self, "plot{}_size".format(i), tk.IntVar())
            plot_size_entry = tk.Entry(
                master=save_settings,
                textvariable=getattr(self, "plot{}_size".format(i)),
                validate="all",
                validatecommand=(validate_int, "%P"),
                disabledbackground=c.PAGE_BG
            )
            setattr(self, "plot{}_size_entry".format(i), plot_size_entry)

            # Label + entry (dropdown) for extension of the file
            plot_extension_label = tk.Label(
                master=save_settings,
                text="Select file extension:",
                **c.PAGE_LABEL_SETTINGS
            )
            setattr(self, "plot{}_extension_label".format(i), plot_extension_label)
            setattr(self, "plot{}_extension".format(i), tk.StringVar())
            choices = ["png", "jpeg", "gif", "tif"]
            plot_extension_entry = ttk.OptionMenu(
                save_settings,
                getattr(self, "plot{}_extension".format(i)),
                choices[0],
                *choices,
                style="select_extension.TMenubutton"
            )
            setattr(self, "plot{}_extension_entry".format(i), plot_extension_entry)

            plot_extension_entry.nametowidget(plot_extension_entry.cget("menu")).configure(bg="#9acdd9")

            plot_name_label.grid(row=0, column=0, sticky="nse", pady=5)
            plot_name_entry.grid(row=0, column=1, sticky="nsw", padx=(10, 0), pady=5)
            plot_size_label.grid(row=2, column=0, sticky="nse", pady=5)
            plot_size_entry.grid(row=2, column=1, sticky="nsw", padx=(10, 0), pady=5)
            plot_extension_label.grid(row=3, column=0, sticky="nse", pady=5)
            plot_extension_entry.grid(row=3, column=1, sticky="nsw", padx=(10, 0), pady=5)

            checkbox.grid(row=i, column=0, padx=5, pady=5)
            plot.grid(row=i, column=1, padx=5, pady=5, sticky="nsw")
            save_settings.grid(row=i, column=2, padx=5, pady=5)

            checkbox.invoke()

        # Make frame to hold buttons
        buttons = tk.Frame(container, bg=c.PAGE_BG)

        # Make buttons for in buttons frame
        select_dir_button = styling.PageButton(
            master=buttons,
            text="Select dir",
            command=self.select_dir
        )
        self.save_button = styling.PageButton(
            master=buttons,
            text="Save selected images",
            command=self.save_plots
        )
        self.save_button.configure(state="disabled")

        # Place buttons in buttons frame
        select_dir_button.grid(row=0, column=0, padx=5)
        self.save_button.grid(row=0, column=1, padx=5)

        buttons.grid(row=i+1, column=0, columnspan=10)

        container.update()

    def _checkbox_action(self):
        """
        :return: None
        Method that will be called when a checkbox is clicked
        Checks if a checkbox (the value connected to it) is true or false
        If it is True it will make the text 'active color', and set the entries to state='normal'
        If it is False it will make the text 'disabled color' and set the entries to state='disabled'
        """
        for i in range(len(self.plots)):
            if getattr(self, "plot{}_bool".format(i)).get():
                getattr(self, "plot{}_name_label".format(i)).configure(fg=c.PAGE_FG)
                getattr(self, "plot{}_size_label".format(i)).configure(fg=c.PAGE_FG)
                getattr(self, "plot{}_extension_label".format(i)).configure(fg=c.PAGE_FG)

                getattr(self, "plot{}_name_entry".format(i)).configure(state="normal")
                getattr(self, "plot{}_size_entry".format(i)).configure(state="normal")
                getattr(self, "plot{}_extension_entry".format(i)).configure(state="normal")

                self.plots[i][1] = True
            else:
                getattr(self, "plot{}_name_label".format(i)).configure(fg="grey")
                getattr(self, "plot{}_size_label".format(i)).configure(fg="grey")
                getattr(self, "plot{}_extension_label".format(i)).configure(fg="grey")

                getattr(self, "plot{}_name_entry".format(i)).configure(state="disabled")
                getattr(self, "plot{}_size_entry".format(i)).configure(state="disabled")
                getattr(self, "plot{}_extension_entry".format(i)).configure(state="disabled")

                self.plots[i][1] = False

    def select_dir(self):
        """
        :return: None
        Method lets the user select a directory, save it in self.dir_path
        if a directory is selected activates the save button
        """
        self.dir_path = tkfile.askdirectory()
        if self.dir_path:
            self.save_button.configure(state="normal")
        else:
            self.save_button.configure(state="disabled")

        # Bring self to top + focus
        SaveImages.active[self.plot_paths].lift()
        SaveImages.active[self.plot_paths].focus_force()

    def save_plots(self):
        """
        :return: None
        Save the plots that have their checkboxes checked (self.plots[i][1] is True) with the given
        values for name, size and extension
        """
        saved = False
        for i in range(len(self.plots)):
            if self.plots[i][1]:
                saved = True
                name = getattr(self, "plot{}_name".format(i)).get()
                size = getattr(self, "plot{}_size".format(i)).get()
                extension = getattr(self, "plot{}_extension".format(i)).get()

                if not name:
                    name = "plot_{}".format(i)
                if not size:
                    size = 600

                fp = "{}/{}.{}".format(self.dir_path, name, extension)
                plot_img = self.plots[i][0].resize((size, size), resample=Image.ANTIALIAS)
                if extension == "jpeg":
                    plot_img = plot_img.convert("RGB")
                plot_img.save(fp)
        if saved:
            msgbox.showinfo(
                title="Message",
                message="Plots saved in {}".format(
                    functions.shorten_dir_name(self.dir_path)
                )
            )
            self._close_window_proc()
        else:
            msgbox.showinfo(
                title="Message",
                message="No plots selected to save"
            )
            # Bring self to top + focus
            SaveImages.active[self.plot_paths].lift()
            SaveImages.active[self.plot_paths].focus_force()

    def _close_window_proc(self):
        """
        :return: None
        Method that will be executed when the window is closed
        removes self.plot_paths from SaveImages.active
        """
        del SaveImages.active[self.plot_paths]
        self.destroy()

    @staticmethod
    def open(plots):
        """
        :param plots: A list of paths to images (plots) to be saved
        :return: None
        Opens instance of this class (SaveImages) if plots not already in SaveImages.active
        If object is made, appends plots to SaveImages.active
        """
        if plots in SaveImages.active.keys():
            # Bring window to foreground
            SaveImages.active[plots].lift()
            SaveImages.active[plots].focus_force()
            return None
        else:
            # Create instance
            instance = SaveImages(plots)
            SaveImages.active[plots] = instance
            SaveImages.active[plots].focus_force()
            return instance
