#!usr/bin/env python3
"""

"""

__author__ = "Naomi"
__version__ = "2019-12-18"

import tkinter as tk
import tkinter.ttk as ttk

import constants as c
import other_windows.screen_events as se


class HelpWindow(se.ScreenEvent):
    """
    Help window to explain program to user
    When this window is closed HelpWindow.active should be set to False
    """

    active = False

    def __init__(self):
        super().__init__(c.APP, height=300, width=200)
        self.change_active(True)
        self.protocol('WM_DELETE_WINDOW', lambda: self.change_active(False))

        self.title("Help")
        self.minsize(200,200)

        msg = tk.Label(self, text="Help")
        msg.pack()

    def change_active(self, set_to):
        HelpWindow.active = set_to
        if set_to == False:
            self.destroy()

    @staticmethod
    def get_help():
        if HelpWindow.active:
            # Bring other_windows window to foreground
            pass
        else:
            HelpWindow()
