#!usr/bin/env python3
"""
Pop up window to ask the user where and how to save the file
"""

__author__ = "Naomi"
__version__ = "2019-12-18"

import tkinter as tk
import tkinter.ttk as ttk

import constants as c


class ScreenEvent(tk.Toplevel):

    pass