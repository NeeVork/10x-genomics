#!usr/bin/env python3
"""Startpage"""

__author__ = "Naomi"
__version__ = "2020-01-16"


import subprocess
import math

import tkinter as tk
import tkinter.filedialog as tkfile

import styling
import constants as c
import functions
from pages.page_one import Page1


class StartPage(styling.Page):

    def __init__(self, master, controller):
        super().__init__(master, name="Dimension reduction")
        self.controller = controller

        # Make rows for in scrollable frame
        self.row_1 = OpenDir(self.scrollable_frame, self)
        self.row_2 = FilterData(self.scrollable_frame, self)
        self.row_3 = MakePCAPlot(self.scrollable_frame, self)
        self.row_4 = IdentifyClusters(self.scrollable_frame, self)

        # Place all the rows with grid
        self.row_1.grid(row=1, column=0, sticky="nsew")
        self.row_2.grid(row=2, column=0, sticky="nsew")
        self.row_3.grid(row=3, column=0, sticky="nsew")
        self.row_4.grid(row=4, column=0, sticky="nsew")

        # Update scrollable window to make sure scrollbar works
        self.scrollable_frame.update()


class OpenDir(styling.Row):

    #dir_name = None
    valid_dir = True

    def __init__(self, master, controller):
        super().__init__(master=master, min_max=False, header_text="Open directory")
        self.content.grid_columnconfigure(1, weight=1)

        self.controller = controller
        self.dir_name = str()
        self.prev_dir = str()

        header = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_LABEL_SETTINGS,
            text="Load a directory containing the matrix.mtx, genes.tsv (or features.tsv), "
                 "and barcodes.tsv files as provided by 10X.")

        open_dir_button = styling.OpenFileButton(
            master=self.content,
            command=self.open_dir
        )

        self.dir_name_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_LABEL_SETTINGS,
            text="Selected directory: No directory selected"
        )

        self.dir_warning_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_WARNING_SETTINGS,
            text="Warning message"
        )

        self.load_data_button = styling.PageButton(
            master=self.content,
            text="Load data",
            command=lambda: functions.thread_task(self.load_data)
        )
        self.load_data_button.config(state=tk.DISABLED)

        self.loaded_data_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_LABEL_SETTINGS,
            text="Loaded data: No data loaded"
        )


        header.grid(row=0, column=0, columnspan=2, sticky="nswe")
        open_dir_button.grid(row=1, column=0, pady=10, sticky="nw")
        self.dir_name_label.grid(row=1, column=1, padx=10, sticky="nsew")
        self.dir_warning_label.grid(row=2, column=0, columnspan=2, sticky="nswe")
        self.load_data_button.grid(row=3, column=0, pady=10, sticky="nw")
        self.loaded_data_label.grid(row=3, column=1, padx=10, sticky="nsew")

        self.dir_warning_label.grid_remove()


    def open_dir(self):
        """
        Method to ask user for directory and set self.dir_name to the path of that directory
        Display selected directory path in self.dir_name_label, if the ppath is more than 50 characters long it will
        only show the of the path.
        :param dir_name_label: tkinter Label object to display directory name
        :return: None
        """
        # Make user select directory
        self.dir_name = tkfile.askdirectory()

        # Check if directory contains the right files
        self._check_files_in_dir()

        # Make the text to be displayed in self.dir_name_label
        if not self.dir_name:
            # If there is no selected dir set text to "No directory selected"
            show_dir_name = "No directory selected"
        else:
            show_dir_name = functions.shorten_dir_name(self.dir_name)

        # Configure dir_name_label to display the dir path to be shown
        self.dir_name_label.config(text="Selected directory: {}".format(show_dir_name))

    def _check_files_in_dir(self):
        """
        Method that ckecks if an appropriate directory is selected
        (a directory containing barcodes.tsv genes.tsv and matrix.mtx)
        If the directory is valid set self.valid_dir to True, else set self.valid_dir to False
        :return: None
        """
        checks = {
            "barcodes.tsv": False,
            "genes.tsv": False,
            "features.tsv": False,
            "matrix.mtx": False,
            "other": False
        }

        # If operating system is windows
        if c.OPERATING_SYS == "Windows":
            self._check_files_in_dir_windows(checks)

        # If operating system is Linux
        # ...

        # Check the values in checks dictionary and set self.vallid_dir acoordingly
        if checks["barcodes.tsv"] and (checks["genes.tsv"] or checks["features.tsv"]) and checks["matrix.mtx"] and not checks["other"]:
            OpenDir.valid_dir = True

            self.load_data_button.config(state=tk.NORMAL)
            self.dir_warning_label.grid_remove()

        else:
            OpenDir.valid_dir = False

            self.load_data_button.config(state=tk.DISABLED)
            self.dir_warning_label.config(text="*No valid directory selected, please select other directory")
            self.dir_warning_label.grid()

    def _check_files_in_dir_windows(self, checks):
        """
        Doest the dictionary check for windows (see _check_files_in_dir for more info)
        :param checks: dictionary indicating the files that are present in the directory
        :return: checks dictionary
        """

        p1 = subprocess.run('cmd /c dir /B "{}"'.format(self.dir_name), capture_output=True, text=True)
        files = p1.stdout.split("\n")
        for file in files:
            if file:
                if file in checks.keys():
                    checks[file] = True
                else:
                    checks["other"] = True
            else:
                continue
        return checks

    def load_data(self):
        """
        :return:  None
        """
        self.load_data_button.config(state=tk.DISABLED)
        self.controller.row_2.make_plot_button.config(state=tk.DISABLED)
        self.controller.row_3.make_plot_button.config(state=tk.DISABLED)
        self.controller.row_4.find_marker_button.config(state=tk.DISABLED)
        self.controller.row_4.make_plot_button.config(state=tk.DISABLED)
        self.controller.controller.frames[Page1].row_2.combine_files_button.config(state=tk.DISABLED)
        self.controller.controller.frames[Page1].row_3.make_plot_button.config(state=tk.DISABLED)

        if self.dir_name == self.prev_dir:
            print("Same data loaded")
            load_new_data = False
        elif self.prev_dir:
            print("Data will be overwritten")
            load_new_data = True
        else:
            print("First time data loaded")
            load_new_data = True

        if load_new_data:
            self.prev_dir = self.dir_name

            if c.OPERATING_SYS == "Windows":
                path_to_script = r'{}R\make_pbmc_object.R'.format(c.WD[0:-9])

                subprocess.call(
                    ["cmd", "/c", c.RSCRIPT_EXE, path_to_script,
                     c.TEMP_DIR.name, self.dir_name]
                )

        self.controller.row_2.make_plot_button.config(state=tk.NORMAL)
        self.controller.row_3.make_plot_button.config(state=tk.NORMAL)
        self.controller.row_4.find_marker_button.config(state=tk.NORMAL)
        self.controller.row_4.make_plot_button.config(state=tk.DISABLED)
        self.controller.row_4.identify_clusters.grid_remove()
        self.controller.controller.frames[Page1].row_2.combine_files_button.config(state=tk.DISABLED)
        self.controller.controller.frames[Page1].row_3.make_plot_button.config(state=tk.DISABLED)
        self.master.update()

        if load_new_data:
            show_dir_name = functions.shorten_dir_name(self.dir_name)
            self.loaded_data_label.configure(text="Selected directory: {}".format(show_dir_name))

        self.load_data_button.config(state=tk.NORMAL)


class FilterData(styling.Row):

    # Variable to store self.radio_v to make it available outside class
    # has this weird construction because tk.StringVar objects didn't work here
    # May not want to use this -> class can only be used once
    use_default = True

    def __init__(self, master, controller):
        super().__init__(master=master, min_max=True, header_text="Filtering data")

        # Configure column weight for scaling page
        self.content.grid_columnconfigure(1, weight=1)

        # Variable to store output from radiobutton
        self.use_default= tk.BooleanVar()
        self.min_feature = tk.IntVar()
        self.max_feature = tk.IntVar()
        self.max_mt = tk.DoubleVar()

        validate_int = self.register(functions.is_int)
        validate_float = self.register(functions.is_float)

        # Make frame with radiobutton for default filtering (radiobutton + labels)
        radio_1 = tk.Frame(master=self.content, bg=c.PAGE_BG)

        radio_1_button = tk.Radiobutton(
            master=radio_1,
            variable=self.use_default, value=True, command=self.radio,
            **c.RADIOBUTTON_CONFIG
        )
        radio_1_label = tk.Label(
            master=radio_1,
            text="Default filtering",
            **c.PAGE_LABEL_SETTINGS
        )

        # Make info frame and fill with widgets for radio_1
        info_frame = tk.Frame(master=radio_1, bg=c.PAGE_BG)
        self.radio_1_labels = list()
        for i, text in enumerate(["Minimum feature count", "Maximum feature count", "Maximum mitochondrial counts", "200", "2500", "5%"]):
            info = tk.Label(master=info_frame, text=text, **c.PAGE_INFO_LABEL_SETTINGS)
            self.radio_1_labels.append(info)
            info.grid(row=i%3, column=math.floor(i/3), sticky="nsw", padx=(20, 0))

        # Fill radio_1 with widgets (radiobutton, label, info)
        radio_1_button.grid(row=0, column=0, sticky="nsw")
        radio_1_label.grid(row=0, column=1, sticky="nsw")
        info_frame.grid(row=1, column=0, sticky="nsw", columnspan=4, padx=(50, 0))

        # Configure columns for scaling page
        radio_1.columnconfigure(2, weight=1)

        # Make frame with radiobutton for custom filtering (radiobutton + labels + entry for options)
        radio_2 = tk.Frame(self.content, bg=c.PAGE_BG)

        radio_2_button = tk.Radiobutton(
            master=radio_2,
            variable=self.use_default, value=False, command=self.radio,
            **c.RADIOBUTTON_CONFIG
        )
        radio_2_label = tk.Label(
            master=radio_2,
            text="Custom filtering",
            **c.PAGE_LABEL_SETTINGS
        )

        # Make info frame and fill with widgets for radio_2
        info_frame = tk.Frame(master=radio_2, bg=c.PAGE_BG)
        self.radio_2_labels = list()
        for i, text in enumerate(["Minimum feature count", "Maximum feature count", "Maximum mitochondrial counts"]):
            info = tk.Label(master=info_frame, text=text, **c.PAGE_INFO_LABEL_SETTINGS)
            self.radio_2_labels.append(info)
            info.configure(**c.PAGE_INFO_DISABLED)
            info.grid(row=i, column=math.floor(i / 3), sticky="nsw", padx=(20, 0))

        choose_min_feature = tk.Entry(
            master=info_frame,
            textvariable=self.min_feature,
            validate="all",
            validatecommand=(validate_int, "%P")
        )
        choose_max_feature = tk.Entry(
            master=info_frame,
            textvariable=self.max_feature,
            validate="all",
            validatecommand=(validate_int, "%P")
        )
        choose_max_mt = tk.Entry(
            master=info_frame,
            textvariable=self.max_mt,
            validate="all",
            validatecommand=(validate_float, "%P")
        )

        self.radio_2_entries = [choose_min_feature, choose_max_feature, choose_max_mt]
        for i, entry in enumerate(self.radio_2_entries):
            entry.configure(state="disabled")
            entry.grid(row=i, column=1, sticky="nsw", padx=(20, 0))

        # Fill radio_2
        radio_2_button.grid(row=0, column=0, sticky="nsw")
        radio_2_label.grid(row=0, column=1, sticky="nsw")
        info_frame.grid(row=1, column=0, sticky="nsw", columnspan=4, padx=(50, 0))

        # Configure columns for scaling page
        radio_2.columnconfigure(2, weight=1)

        # Place radio_1 and radio_2 frame
        radio_1.grid(row=0, column=0, sticky="nsw")
        radio_2.grid(row=1, column=0, sticky="nsw")

        self.make_plot_button = styling.PageButton(self.content, text="Make plot", command=lambda: functions.thread_task(self.make_vln_plots))
        self.make_plot_button.config(state=tk.DISABLED)
        self.plots_bfr = styling.PlotDisplay(self.content, master, title="Before filtering")
        self.plots_aft = styling.PlotDisplay(self.content, master, title="After filtering")

        self.make_plot_button.grid(row=2, column=0, sticky="nsw", pady=10)
        self.plots_bfr.grid(row=3, column=0, columnspan=2, sticky="nswe", pady=(0, 10))
        self.plots_bfr.grid_remove()
        self.plots_aft.grid(row=4, column=0, columnspan=2, sticky="nswe", pady=(0, 10))
        self.plots_aft.grid_remove()

        # Invoke radiobutton for default filtering
        radio_1_button.invoke()

    def make_vln_plots(self):
        self.make_plot_button.config(state=tk.DISABLED)
        settings = self.get_filter_settings()
        path_to_script = r'{}R\violinplot.R'.format(c.WD[0:-9])

        subprocess.call(
            ["cmd", "/c", c.RSCRIPT_EXE, path_to_script,
             c.TEMP_DIR.name, settings["min_features"], settings["max_features"], settings["max_mt"]]
        )

        self.plots_bfr.delete_plots()
        self.plots_aft.delete_plots()

        for file in c.VIOLIN_PLOT_BFR:
            self.plots_bfr.add_plot(file)
        for file in c.VIOLIN_PLOT_AFT:
            self.plots_aft.add_plot(file)
                
        self.plots_bfr.grid()
        self.plots_aft.grid()
        self.master.update()

        self.make_plot_button.config(state=tk.NORMAL)

    def radio(self):
        FilterData.use_default = self.use_default.get()
        if self.use_default.get():
            for label in self.radio_1_labels:
                label.configure(**c.PAGE_INFO_LABEL_SETTINGS)
            for label in self.radio_2_labels:
                label.configure(**c.PAGE_INFO_DISABLED)
            for entry in self.radio_2_entries:
                entry.configure(state="disabled")
        else:
            for label in self.radio_1_labels:
                label.configure(**c.PAGE_INFO_DISABLED)
            for label in self.radio_2_labels:
                label.configure(**c.PAGE_INFO_LABEL_SETTINGS)
            for entry in self.radio_2_entries:
                entry.configure(state="normal")

    def get_filter_settings(self):
        if self.use_default.get():
            settings = {
                "min_features": "200",
                "max_features": "2500",
                "max_mt": "5"
            }
            return settings
        else:
            settings = {
                "min_features": str(self.min_feature.get()),
                "max_features": str(self.max_feature.get()),
                "max_mt": str(self.max_mt.get())
            }
            return settings


class MakePCAPlot(styling.Row):

    def __init__(self, master, controller):
        super().__init__(master=master, min_max=True, header_text="PCA elbow plot")
        self.master = master
        self.controller = controller
        self.amount_pca = tk.IntVar()

        validate = self.register(functions.is_int)

        self.content.grid_columnconfigure(2, weight=1)

        self.choose_pca = tk.Entry(
            self.content,
            textvariable=self.amount_pca,
            validate="all",
            validatecommand=(validate, "%P")
        )

        self.make_plot_button = styling.PageButton(
            self.content,
            text="Make elbow plot for PCA",
            command= lambda: functions.thread_task(self.make_pca_elbow_plot))
        self.make_plot_button.config(state=tk.DISABLED)

        self.choose_pca.grid(row=0, column=0, pady=(0, 10), sticky="nsew")
        self.make_plot_button.grid(row=0, column=1, pady=(0, 10), padx=20, sticky="nsew")

        self.plots = styling.PlotDisplay(self.content, master)

        self.warning_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_WARNING_SETTINGS,
            text="Warning message"
        )

        self.warning_label.grid(row=3, column=0, columnspan=3, sticky="nsew")
        self.plots.grid(row=4, column=0, columnspan=3, sticky="nsew", pady=(0, 10))

        self.warning_label.grid_remove()
        self.plots.grid_remove()

    def make_pca_elbow_plot(self):
        self.make_plot_button.config(state=tk.DISABLED)
        path_to_script = r'{}R\pca_elbow.R'.format(c.WD[0:-9])
        try:
            if self.amount_pca.get() < 20:
                use_amount_pca = "20"
            elif self.amount_pca.get() > 50:
                use_amount_pca = "50"
            else:
                use_amount_pca = str(self.amount_pca.get())

            settings = self.controller.row_2.get_filter_settings()

            functions.run_r_script(
                path_to_script,
                c.TEMP_DIR.name, use_amount_pca,
                settings["min_features"], settings["max_features"], settings["max_mt"]
            )

            self.plots.delete_plots()
            self.plots.add_plot(c.ELBOW_PLOT)
            self.plots.grid()
            self.master.update()

        except tk.TclError:
            self.warning_label.configure(text="*Please enter an integer value")
            self.warning_label.grid()

        self.make_plot_button.config(state=tk.NORMAL)


class IdentifyClusters(styling.Row):

    def __init__(self, master, controller):
        super().__init__(master=master, header_text="Identify clusters in data")
        self.master = master
        self.controller = controller

        self.content.grid_columnconfigure(3, weight=1)

        self.amount_pca = tk.IntVar()
        self.resolution = tk.DoubleVar()
        self.settings = dict()
        self.cluster_gene_dict = dict()
        self.prev_dir = str()

        self.amount_pca.set(20)
        self.resolution.set(0.5)

        validate_int = self.register(functions.is_int)
        validate_float = self.register(functions.is_float)

        pca_label = tk.Label(
            master=self.content,
            text="Choose how many PC's to use",
            **c.PAGE_LABEL_SETTINGS
        )
        pca_entry = tk.Entry(
            master=self.content,
            textvariable=self.amount_pca,
            validate="all",
            validatecommand=(validate_int, "%P")
        )
        resolution_label = tk.Label(
            master=self.content,
            text="Choose resolution",
            **c.PAGE_LABEL_SETTINGS
        )
        resolution_entry = tk.Entry(
            master=self.content,
            textvariable=self.resolution,
            validate="all",
            validatecommand=(validate_float, "%P")
        )

        self.find_marker_button = styling.PageButton(
            master=self.content,
            text="Reduce dimensions",
            command=lambda: functions.thread_task(self.find_marker_genes)
        )
        self.find_marker_button.configure(state=tk.DISABLED)
        self.make_plot_button = styling.PageButton(
            master=self.content,
            text="Make t-SNE",
            command=lambda: functions.thread_task(self.make_plot_tsne)
        )
        self.make_plot_button.configure(state=tk.DISABLED)
        self.identify_clusters = IdentifyClustersEnterNames(
            master=self.content
        )
        self.plots = styling.PlotDisplay(
            master=self.content,
            update=master
        )

        pca_label.grid(row=0, column=0, sticky="nw", pady=5)
        pca_entry.grid(row=0, column=1, sticky="nw", pady=5, padx=(10, 0))
        resolution_label.grid(row=1, column=0, sticky="nw", pady=5)
        resolution_entry.grid(row=1, column=1, sticky="nw", pady=5, padx=(10, 0))
        self.find_marker_button.grid(row=2, column=0, sticky="nw", pady=10)
        self.identify_clusters.grid(row=3, column=0, columnspan=10, sticky="nsew")
        self.make_plot_button.grid(row=4, column=0, sticky="nw", pady=10)
        self.plots.grid(row=5, column=0, columnspan=5, sticky="nswe", pady=(0, 10))

        self.identify_clusters.grid_remove()
        self.plots.grid_remove()

    def find_marker_genes(self):
        """
        Method that runs an Rscript to cluster data and find marker genes.
        Opens entries for identifying the clusters.
        :return:
        """
        self.find_marker_button.configure(state=tk.DISABLED)
        self.make_plot_button.configure(state=tk.DISABLED)

        new_run = "TRUE"
        settings = self.controller.row_2.get_filter_settings()
        if settings == self.settings and self.prev_dir == self.controller.row_1.dir_name:
            new_run = "FALSE"
        self.settings = settings
        if self.amount_pca.get() > 50:
            amount_pca = 50
        elif self.amount_pca.get() < 2:
            amount_pca = 2
        else:
            amount_pca = self.amount_pca.get()

        if self.resolution.get() > 10:
            resolution = 10
        elif self.resolution.get() < 0:
            resolution = 0
        else:
            resolution = self.resolution.get()

        path_to_script = r'{}R\identify_clusters.R'.format(c.WD[0:-9])
        functions.run_r_script(
            path_to_script, c.TEMP_DIR.name, amount_pca, resolution,
            settings["min_features"], settings["max_features"], settings["max_mt"], new_run
        )

        self.prev_dir = self.controller.row_1.dir_name

        self.cluster_gene_dict = dict()

        self.parse_file("{}/cluster_marker_gene.txt".format(c.TEMP_DIR.name))

        self.identify_clusters.add_entry_fields(self.cluster_gene_dict)
        self.identify_clusters.grid()
        self.master.update()
        self.make_plot_button.configure(state=tk.NORMAL)
        if self.controller.controller.frames[Page1].ready_for_combine:
            self.controller.controller.frames[Page1].row_2.combine_files_button.config(state=tk.NORMAL)

        self.find_marker_button.configure(state=tk.NORMAL)

    def parse_file(self, file_name):
        with open(file_name) as in_file:
            features = in_file.read()
            features_list = features.split("\n\n")
            clusters = features_list[0].split("\t")
            marker_genes = features_list[1].split("\t")
            for cluster, gene in zip(clusters, marker_genes):
                if cluster in self.cluster_gene_dict.keys():
                    self.cluster_gene_dict[cluster].append(gene)
                else:
                    self.cluster_gene_dict[cluster] = [gene]

    def make_plot_tsne(self):
        self.make_plot_button.config(state=tk.DISABLED)
        path_to_script = r'{}R\make_tsne.R'.format(c.WD[0:-9])
        cluster_names = ",!".join([x.get() for x in self.identify_clusters.variables])

        functions.run_r_script(
            path_to_script, c.TEMP_DIR.name, cluster_names
        )

        self.plots.delete_plots()
        self.plots.add_plot(c.TSNE_1)
        self.plots.grid()
        self.master.update()
        self.make_plot_button.config(state=tk.NORMAL)

class IdentifyClustersEnterNames(styling.BorderedFrame):

    def __init__(self, master):
        super().__init__(master=master,
                         bd_top=1, bd_right=1, bd_bottom=1, bd_left=1, bd_color="white",
                         inner_widget=tk.Frame, pack_options=dict(), bg=c.PAGE_BG)

        self.inner.grid_columnconfigure(3, weight=1)

        self.dynamic_widgets = list()
        self.variables = list()

    def add_entry_fields(self, cluster_gene_dict):
        """
        :param cluster_gene_dict:
        :return:
        """
        # Remove old data (if it exists)
        for widget in self.dynamic_widgets:
            widget.grid_forget()
        self.dynamic_widgets = list()
        self.variables = list()

        # For every cluster in the cluster_genes_dict, make a label with cluster (number), a label with the marker
        # genes (append both labels to self.labels), an entry field and store the variable connected tot the entry field
        # in a tk variable, and append tk variable (tk.StringVar) to the self.variables list
        for cluster in cluster_gene_dict:
            cluster_var = tk.StringVar()
            self.variables.append(cluster_var)

            border_row = styling.BorderedFrame(
                master=self.inner,
                bd_top=0, bd_right=0, bd_bottom=1, bd_left=0, bd_color="white",
                inner_widget=tk.Frame, pack_options=dict(), bg=c.PAGE_BG
            )

            cluster_label = styling.BorderedFrame(
                master=border_row.inner,
                bd_top=0, bd_right=1, bd_bottom=0, bd_left=0, bd_color="white",
                inner_widget=tk.Label, pack_options=dict(), **c.PAGE_LABEL_SETTINGS,
                text=cluster, width=5
            )
            marker_genes_label = tk.Label(
                master=border_row.inner,
                text=", ".join(cluster_gene_dict[cluster]),
                **c.PAGE_LABEL_SETTINGS
            )
            cluster_name_entry = tk.Entry(
                master=border_row.inner,
                textvariable=cluster_var
            )

            cluster_name_entry.default_text = True
            cluster_name_entry.bind("<Button-1>", functions.delete_text)
            cluster_name_entry.insert(0, "Enter cluster name")

            self.dynamic_widgets.append(cluster_label)
            self.dynamic_widgets.append(marker_genes_label)
            self.dynamic_widgets.append(cluster_name_entry)
            self.dynamic_widgets.append(border_row)

            cluster_label.grid(row=0, column=0, sticky="nw", pady=5)
            cluster_name_entry.grid(row=0, column=1, sticky="ne", pady=5, padx=(15, 0))
            marker_genes_label.grid(row=0, column=2, sticky="new", pady=5, padx=(15, 0))

            border_row.grid(row=int(cluster), column=0, sticky="nsew", columnspan=10)
