#!usr/bin/env python3
"""Page one"""

__author__ = "Naomi"
__version__ = "2020-01-16"

import subprocess
import os
import shutil

import tkinter as tk
import tkinter.filedialog as tkfile

import styling
import constants as c
import functions
import pages.startpage as sp


class Page1(styling.Page):

    def __init__(self, master, controller):
        super().__init__(master, name="Analyze clonotypes")
        self.controller = controller
        self.ready_for_combine = False

        # Make rows for in scrollable frame
        self.row_1 = OpenFile(self.scrollable_frame, self)
        self.row_2 = CombineFiles(self.scrollable_frame, self)
        self.row_3 = HighlightTopFive(self.scrollable_frame, self)

        # Place all the rows with grid
        self.row_1.grid(row=1, column=0, sticky="nsew")
        self.row_2.grid(row=2, column=0, sticky="nsew")
        self.row_3.grid(row=3, column=0, sticky="nsew")

        # Update scrollable window to make sure scrollbar works
        self.scrollable_frame.update()

class OpenFile(styling.Row):

    def __init__(self, master, controller):
        super().__init__(master=master, header_text="Open file")
        self.content.grid_columnconfigure(1, weight=1)

        self.master = master
        self.controller = controller
        self.file_name = str()
        self.amount_clonotype = tk.StringVar()

        validate_int = self.register(functions.is_int)

        header = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_LABEL_SETTINGS,
            text="Load a \"Filtered contig annotations (CSV)\" as provided by 10X."
        )
        open_file_button = styling.OpenFileButton(
            master=self.content,
            command=self.get_file_name
        )
        self.file_name_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_LABEL_SETTINGS,
            text="Selected file: No file selected"
        )
        self.file_warning_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_WARNING_SETTINGS,
            text="Warning message"
        )
        amount_clonotype_label = tk.Label(
            master=self.content,
            text="Top .. clonotypes in plot",
            **c.PAGE_LABEL_SETTINGS
        )
        amount_clonotype_entry = tk.Entry(
            master=self.content,
            textvariable=self.amount_clonotype,
            validate="all",
            validatecommand=(validate_int, "%P")
        )
        self.make_plot_button = styling.PageButton(
            master=self.content,
            text="Load data",
            command=self.make_barplot
        )
        self.make_plot_button.config(state=tk.DISABLED)
        make_plot_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_LABEL_SETTINGS,
            text="Load data and make plot of clonotype frequencies"
        )
        self.plots = styling.PlotDisplay(
            master=self.content,
            update=master,
            title="Clonotype frequencies"
        )

        header.grid(row=0, column=0, columnspan=2, sticky="nswe")
        open_file_button.grid(row=1, column=0, pady=10, sticky="nw")
        self.file_name_label.grid(row=1, column=1, padx=10, sticky="nsew")
        self.file_warning_label.grid(row=2, column=0, columnspan=2, sticky="nswe")
        amount_clonotype_label.grid(row=3, column=0, padx=10, sticky="nsew")
        amount_clonotype_entry.grid(row=3, column=1, padx=10, sticky="nw")
        self.make_plot_button.grid(row=4, column=0, pady=10, sticky="nw")
        make_plot_label.grid(row=4, column=1, padx=10, sticky="nsew")
        self.plots.grid(row=5, column=0, columnspan=2, sticky="nswe", pady=(0, 10))


        self.file_warning_label.grid_remove()
        self.plots.grid_remove()

    def get_file_name(self):
        self.file_name = tkfile.askopenfilename()

        if self.check_csv(self.file_name):
            self.file_warning_label.grid_remove()
            show_file_name = functions.shorten_dir_name(self.file_name)
            self.file_name_label.configure(text="Selected file: {}".format(show_file_name))
            self.make_plot_button.config(state=tk.NORMAL)
        else:
            self.file_warning_label.config(text="*Please select a .csv file")
            self.file_warning_label.grid()
            self.make_plot_button.config(state=tk.DISABLED)

    def check_csv(self, file_name):
        if len(file_name) > 2:
            extension = file_name[-4:].lower()
            if extension == ".csv":
                return True
        return False

    def make_barplot(self):

        if not self.amount_clonotype.get():
            self.amount_clonotype.set(10)

        path_to_script = r'{}R\histogram.R'.format(c.WD[0:-9])

        subprocess.call(
            ["cmd", "/c", c.RSCRIPT_EXE, path_to_script,
             c.TEMP_DIR.name, self.file_name, self.amount_clonotype.get()]
        )

        self.plots.delete_plots()
        self.plots.add_plot(c.BARPLOT_CLONOTYPES)
        self.plots.grid()
        self.master.update()

        self.controller.ready_for_combine = True

        if str(self.controller.controller.frames[sp.StartPage].row_4.make_plot_button['state']) == "normal":
            self.controller.row_2.combine_files_button.config(state=tk.NORMAL)


class CombineFiles(styling.Row):

    def __init__(self, master, controller):
        super().__init__(master=master, header_text="Combine data")
        self.content.grid_columnconfigure(1, weight=1)

        self.master = master
        self.controller = controller

        self.combine_files_button = styling.PageButton(
            master=self.content,
            text="Combine data",
            command=lambda: functions.thread_task(self.combine_files)
        )
        self.combine_files_button.config(state=tk.DISABLED)
        self.warning_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_WARNING_SETTINGS,
            text="Warning message"
        )
        self.plots = styling.PlotDisplay(
            master=self.content,
            update=master,
            title="Cells from contig file highlighted"
        )
        self.clono_filtered_label = styling.WrappingLabel(
            master=self.content,
            **c.PAGE_LABEL_SETTINGS
        )

        self.combine_files_button.grid(row=0, column=0, sticky="nswe", pady=(0, 10))
        self.warning_label.grid(row=1, column=0, columnspan=2, sticky="nswe")
        self.plots.grid(row=2, column=0, columnspan=2, sticky="nswe", pady=(0, 10))
        self.clono_filtered_label.grid(row=3, column=0, columnspan=2, sticky="nswe")

        self.plots.grid_remove()
        self.warning_label.grid_remove()
        self.clono_filtered_label.grid_remove()

    def combine_files(self):
        self.combine_files_button.config(state=tk.DISABLED)
        self.controller.row_3.make_plot_button.configure(state=tk.DISABLED)
        self.warning_label.grid_remove()
        self.plots.delete_plots()
        self.plots.grid_remove()
        self.clono_filtered_label.grid_remove()
        self.update()
        self.master.update()
        try:
            os.remove(c.TSNE_HIGHLIGHTED)
        except FileNotFoundError:
            pass

        path_to_script = r'{}R\combine_data.R'.format(c.WD[0:-9])

        subprocess.call(
            ["cmd", "/c", c.RSCRIPT_EXE, path_to_script,
             c.TEMP_DIR.name]
        )

        try:
            self.plots.add_plot(c.TSNE_HIGHLIGHTED)
            self.plots.add_plot(c.BARPLOT_CLONOTYPES_COMPARE)
            self.plots.grid()
            self.get_amount_filtered()
            self.clono_filtered_label.grid()
            self.master.update()
            self.controller.row_3.make_plot_button.configure(state=tk.NORMAL)
        except FileNotFoundError:
            self.warning_label.configure(text="*Could not combine files. Make sure to use matching files.")
            self.warning_label.grid()
            self.master.update()

        self.combine_files_button.config(state=tk.NORMAL)

    def get_amount_filtered(self):
        file_name = "{}/total_clonotypes_filtered.txt".format(c.TEMP_DIR.name)
        with open(file_name) as open_file:
            amount = open_file.read()
            self.clono_filtered_label.configure(text="Amount of clonotypes lost by filtering step: {}".format(amount))


class HighlightTopFive(styling.Row):

    def __init__(self, master, controller):
        super().__init__(master=master, header_text="Differential gene expression largest clonotypes")
        self.content.grid_columnconfigure(2, weight=1)

        self.master = master
        self.controller = controller

        self.make_plot_button = styling.PageButton(
            master=self.content,
            text="Make plots",
            command=lambda: functions.thread_task(self.make_highlighted_plot)
        )
        self.make_plot_button.configure(state=tk.DISABLED)
        self.plots = styling.PlotDisplay(
            master=self.content,
            update=master,
            title="Top five clonotypes highlighted"
        )
        self.export_csv_button = styling.PageButton(
            master=self.content,
            text="Export marker genes",
            command=self.export_csv
        )
        self.export_csv_button.configure(state=tk.DISABLED)

        self.make_plot_button.grid(row=0, column=0, sticky="nswe", pady=(0, 10))
        self.export_csv_button.grid(row=0, column=1, sticky="nswe", pady=(0, 10))
        self.plots.grid(row=1, column=0, columnspan=3, sticky="nswe", pady=(0, 10))

        self.plots.grid_remove()

    def make_highlighted_plot(self):
        self.make_plot_button.config(state=tk.DISABLED)
        self.export_csv_button.configure(state=tk.DISABLED)
        self.plots.delete_plots()
        self.plots.grid_remove()
        self.update()
        self.master.update()

        path_to_script = r'{}R\mark_top_5.R'.format(c.WD[0:-9])

        subprocess.call(
            ["cmd", "/c", c.RSCRIPT_EXE, path_to_script,
             c.TEMP_DIR.name]
        )

        for i in range(1, 6):
            self.plots.add_plot(getattr(c, "TSNE_TOP5_{}".format(i)))
        self.plots.grid()
        self.master.update()
        self.export_csv_button.configure(state=tk.NORMAL)
        self.make_plot_button.config(state=tk.NORMAL)

    def export_csv(self):
        old_file = "{}/marker_genes.csv".format(c.TEMP_DIR.name)
        new_file = tkfile.asksaveasfilename(filetypes=[('comma seperated values', '*.csv')], defaultextension=".csv")
        shutil.copyfile(old_file, new_file)
