#!/usr/bin/env python3
"""

"""

__author__ = "Naomi Hindriks"
__version__ = "2020-01-31"

import sys
import argparse
import os
import subprocess


def get_arguments_with_argparse(args):
    """
    Functie die argumenten die in de terminal gegeven worden parsed met de module argparse
    """
    # Maak argparse parser object
    parser = argparse.ArgumentParser(
        description="Kies gebruik van programma"
    )
    subparsers = parser.add_subparsers(
        help='sub-command help',
        dest="which"
    )

    parser_find = subparsers.add_parser(
        "find",
        help='Vind markergenen van top 5 clonotypes'
    )
    parser_find.add_argument(
        "dir_in",
        help="Naam van gene/cell matrix directory, absolute path"
    )
    parser_find.add_argument(
        "dir_out",
        help="Naam van direcotry om resultaten op te slaan, absolute path"
    )
    parser_find.add_argument(
        "contig_file",
        help="Naam van CSV bestand met informatie over clonotypes van T-cellen, absolute path"
    )
    parser_find.add_argument(
        "-d", "--dont_save",
        const="True",
        action="store_const",
        help="Gebruik deze flag om geen plotjes te maken"
    )
    parser_find.add_argument(
        "-s",
        "--settings",
        nargs=3,
        help="Geef settings voor het filteren van de data in de volgorde:"
             "minimum feature count, maximum feature count, maximum percentage mitochondrial dna. Geef alleen getallen."
             "Indien deze optie niet gebruikt wordt wordt de default filtering toegepast van:"
             "minimum feature count: 200, maximum feature count: 2500, maximum percentage mitochondrial dna: 5.0"
    )
    parser_find.add_argument(
        "-a",
        "--amount_pc",
        help="Hoeveelheid principle components om te gebruiken voor dimensie reductie."
             "Indien deze optie niet gebruikt wordt wordt de default van 20 gebruikt."
    )
    parser_find.add_argument(
        "-t",
        "--type_plots",
        help="Geef het type plot aan, type tsne of umap."
             "Indien deze optie niet gebruikt wordt worden er UMAP plots gemaakt."
             "Alleen van toepassing als --dont_save optie niet gebruikt wordt."
    )
    parser_find.add_argument(
        "-f",
        "--find_markers",
        const="True",
        action="store_const",
        help="Gebruik deze flag om een file in de output directory te maken met voor elk gevonden cluster in de"
             "data uit de gene/cell matrix directory een lijst van de top 5 marker genes."
    )

    parser_compare = subparsers.add_parser(
        "compare",
        help="Vergelijk twee CSV bestanden met markergenen, maakt een csv bestand van marker genen in file_1 die "
             "niet voorkomen in file_2, en een csv bestand van marker genes die in file_1 voorkomen en ook"
             "in file_2 voorkomen. Deze bestanden worden opgeslagen in de meegegeven directory"
    )
    parser_compare.add_argument(
        "file_1",
        help="Naam van CSV bestand met markergenen geproduceerd door de find optie van dit programma, absolute path"
    )
    parser_compare.add_argument(
        "file_2",
        help="Naam van CSV bestand met markergenen geproduceerd door de find optie van dit programma, absolute path"
    )
    parser_compare.add_argument(
        "dir_out",
        help="Naam van direcotry om resultaten op te slaan, absolute path"
    )
    parser_compare.add_argument(
        "-a", "--alpha",
        help="Maximale adjusted p value voor waardes om terug te geven, dit gaat om de adjusted p value berekend met "
             "een Bonferroni-correctie. Indien deze optie niet wordt gebruikt word er een waarde van 0.05 gebruikt"
    )

    # Return arguments van parser object
    return parser


def main(args):
    """
    Main functie
    """
    parser = get_arguments_with_argparse(args[1:])
    parsed_args = parser.parse_args()

    if parsed_args.which == "find":
        dir_in = parsed_args.dir_in
        dir_out = parsed_args.dir_out
        contig_file = parsed_args.contig_file

        save_plots = "True"
        settings = ["200", "2500", "5"]
        amount_pc = "20"
        type_plot = "umap"
        find_markers = "False"

        if parsed_args.dont_save:
            save_plots = "False"

        if parsed_args.settings:
            settings = parsed_args.settings

        if parsed_args.amount_pc:
            amount_pc = parsed_args.amount_pc

        if parsed_args.type_plots.lower() == "umap":
            type_plot = "umap"
        elif parsed_args.type_plots.lower() == "tsne" or parsed_args.type_plots.lower() == "t-sne":
            type_plot = "tsne"
        else:
            print("Onbekende optie gebruikt voor --type_plots, de default (UMAP) zal gebruikt worden.")

        if parsed_args.find_markers:
            find_markers = "True"

        working_dir = os.getcwd()
        rscript_exe = r'{}R\R-3.6.2\bin\Rscript.exe'.format(working_dir[0:-17])
        script_path = r"{}\complete_pipe.R".format(working_dir)

        subprocess.call(
            ["cmd", "/c", rscript_exe, script_path,
             dir_in, dir_out, save_plots, *settings, amount_pc, type_plot, find_markers, contig_file]
        )

    elif parsed_args.which == "compare":

        file_1 = parsed_args.file_1
        file_2 = parsed_args.file_2
        dir_out = parsed_args.dir_out
        alpha = "0.05"

        if parsed_args.alpha:
            alpha = parsed_args.alpha

        working_dir = os.getcwd()
        rscript_exe = r'{}R\R-3.6.2\bin\Rscript.exe'.format(working_dir[0:-17])
        script_path = r"{}\compare_marker_genes.R".format(working_dir)

        subprocess.call(
            ["cmd", "/c", rscript_exe, script_path,
             file_1, file_2, dir_out, alpha]
        )

    else:
        parser.print_help()

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
