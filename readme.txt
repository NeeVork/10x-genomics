Auteur: James Gray, Ewoud Ooms, Kevin Smit, Naomi Hindriks
Datum: 2020-01-31
Versie: 1


Project Naam: CLAP tenX


######################################################
#################### Omschrijving ####################
######################################################


Programma met twee functies: 
1) gene/cell matrix betsanden van 10x Genomics te koppelen aan CSV bestand met informatie over clonotypes van T-cellen van 10x Genomics en markergenen vinden van de 5 grootste clonotypes. Uikomst exporteren als CSV.
2) Twee csv bestenden gegenereerd met functie 1 met elkaar vergelijken: markergenen die in file 1 voorkomen en niet in file 2 oplsaan in csv bestand en markergenen die voorkomen file 1 en ook in file 2 opslaan in csv bestand.


#####################################################
#################### Installatie ####################
#####################################################

Requirements:

	- Windows 10
	- Python 3 (https://www.python.org/downloads/)
		- Add python3 to path
	- PIL
		- Installatie -> Ga naar opdrachtprompt -> python -m pip3 install Pillow
	


#################################################
#################### Gebruik ####################
#################################################


Voor de eerste functie van het programma (zie omschrijving) kan er zowel gebruikt gemaakt worden van de graphical user interface (GUI) of de commandline tool. Voor de tweede functie van het programma (zie omschrijving) kan enkel gebruik gemaakt worden met de commandline tool.

Gebruik van de GUI:
1- Ga naar het mapje interface, dubbeklik op app.py
2- Op de dimension reductie pagina selecteer een gene/cell matrix map van 10x
3- Druk op de knop 'Load data'
4- Optioneel: voor custom filtering van de data open het 'Filtering data' segment en vul gewenste filtering settings in, vervolgens is er de mogelijkheid om violin plots te maken om de verdeling van de data te inspecteren voor en na het filteren.
5- Optioneel: Voor het maken van een PCA elbow plot voor het controleren van hoeveel invloed principle components hebben kan de 'PCA elbow plot' sectie open geklapt en gebruikt worden.
6- In het 'Identify clusters in data' segment kan de hoeveelheid principle components gekozen worden om te gebruiken en de resolutie voor het vinden van de clusters. Vervolgens kunnen de clusters gevormd worden door op de 'Reduce dimensions knop te drukken'
7- Als de clusters gevormd zijn is er de mogelijkheid elk cluster een naam te geven aand e hand van marker genen gevonden voor dat cluster, en er een t-SNE plot van te maken door op de 'Make t-SNE' knop te drukken
8- Op de pagina 'Analyze clonotypes' kan een CSV bestand met informatie over clonotypes van T-cellen van 10x ingeladen worden. Er wordt vervolgens een staaf grafiek gemaakt van de hoeveelheid van elk clonotypes (In te vullen van de top hoeveel clonotypes de grafiek moet worden gemaakt)
9- Als zowel de clusters gevormd zijn op de 'dimension reduction' pagina (stap 6) en er een CSV bestand met informatie over clonotypes van T-cellen van 10x is ingeladen kan op de 'combine data' knop worden gedrukt. Dan wordt de data gecombineerd en worden er verschillende plotjes gevormd die laten zien hoe de data overlapt.
10-Vervolgens kan er in het 'Differential gene expression largest clonotypes' segment op de knop 'make plots' worden gedrukt, er worden dan meerdere plotjes gemaakt waarop de grootste clonotypes te zijn zijn in een t-SNE plot. Er wordt berekend welke genen omhoog gereguleerd zijn voor elk van de 5 grootste clonotypes tegen over alle andere t-cellen. Deze data kan vervolgens opgeslagen worden door op de 'Export marker genes' knop te drukken, er kan dan een map en bestandsnaam worden geselecteerd en dan wordt er een CSV bestand opgeslagen.

11-Voor alle gegenereerde plotjes kan op de knop 'Save image(s)' gedrukt worden, er opent dan een nieuw venster waarmee de plotjes kunnen worden opgeslagen.

Gebruik de commandline tool:
De commandline tool is opgedeeld in twee subprogrammas, deze heten find en compare.
Voor beide geldt het volgende:
- Navigeer in de opdrachtprompt naar de map waar het programma is opgeslagen en vervolgens naar de map command_line_tool.
- Gebruik python om het programma te openen en evuntueel de help functie aan te roepen, om de help aan te roepen gebruik: python claptenx.py -h


Gebruik van find:
- Om de help functie aan te roepen gebruik: python claptenx.py find -h
- In de help staan alle opties die megegeven moeten of kunnen worden
- Als het gerund wordt wordt er een mapje aan gemaakt in de map aangegeven in de dir_out, met de naam CLAPtenX_datum_tijd, als de optie -d niet gegeven wordt wordt er in deze map een mapje gemaakt die plots heet en hierin worden alle gegenereerde plotjes opgeslagen. Verder worden er in de gemaakte map de gewenste csv bestanden opgeslagen.

Gebruik van compare:
- Om de help functie aan te roepen gebruik: python claptenx.py compare -h
- Als compare gerund moet moeter er twee cvs bestanden meegegeven worden zoals gegenereerd met de find functie van het programma. Verder moet een map gespicificeerd worden waar de resultaten moeten worden opgeslagen. Als laatste is er de optie om zelf de alpha in te stellen voor de weer te geven resultaten.
- Er worden twee csv bestanden gegenereerd. genes_only_in_file_1.csv geeft aan welke genen in file_1 voorkomen en niet in file_2 (file_1 is de als eerst opgegeven file en file_2 de als tweede opgegeven file), en genes_in_both_files.csv geeft aan welke genen in file_1 voorkomen die ook in file_2 voorkomen.

#################################################
#################### Support ####################
#################################################

Voor vragen of opmerkingen mail naar n.j.hindriks@st.hanze.nl

##########################################################
############### Authors and acknowledgment ###############
##########################################################

Met dank aan Astrid Alsema en Jasper Bosman ze hebben ons weten te inspireren om aan dit project te werken. Zonder hun briljante inspiratie hadden wij nooit tot dit resultaat kunnen komen.
