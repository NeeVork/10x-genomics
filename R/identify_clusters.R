##################################################
## Project: CLAP tenX
## Script purpose:
## Date: 2020-12-01
## Author: Kevin, Naomi
##################################################

library(dplyr)
library(Seurat)

args <- commandArgs(trailingOnly = TRUE)

# Set variables form command line
temp.dir <- args[1]
amount.pca <- as.integer(args[2])
resolution <- as.numeric(args[3])
min.feature.count <- as.integer(args[4])
max.feature.count <- as.integer(args[5])
max.percent.mt <- as.numeric(args[6])
new.settings <- as.character(args[7])

rds.file <- paste(temp.dir, "/normalized_PCA_pbmc.rds", sep = "")

if (file.exists(rds.file) & new.settings == "FALSE"){
  # Load data
  pbmc <- readRDS(file = paste(temp.dir, "/normalized_PCA_pbmc.rds", sep = ""))
}else{
  # Load data
  pbmc <- readRDS(file = paste(temp.dir, "/og_pbmc.rds", sep=""))
  
  # Apply filtering on data
  source("../R/filter_data.R")
  
  pbmc <- NormalizeData(pbmc)
  
  # Scale data
  pbmc <- ScaleData(pbmc)
  
  pbmc <- FindVariableFeatures(pbmc, selection.method = "vst", nfeatures = 2000)
  pbmc <- RunPCA(pbmc, features = VariableFeatures(object = pbmc))
  
  # Save pbmc object
  saveRDS(pbmc, file = paste(temp.dir, "/normalized_PCA_pbmc.rds", sep = ""))
}

# Find clusters
pbmc <- FindNeighbors(pbmc, dims = 1:amount.pca)
pbmc <- FindClusters(pbmc, resolution = resolution)
pbmc <- RunTSNE(pbmc, dims = 1:amount.pca)

# Find marker genes of the clusters
pbmc.markers <- FindAllMarkers(pbmc, max.cells.per.ident = 100, 
                               min.diff.pct = 0.3, only.pos = TRUE)
markers <- pbmc.markers %>% group_by(cluster) %>% top_n(n = 5, wt = avg_logFC)

# Save marker genes and clusters to a text file in temp.dir
df <- do.call(rbind.data.frame, list(markers$cluster, markers$gene))
write.table(df, file = paste(temp.dir, "/cluster_marker_gene.txt", sep = ""), quote = FALSE, sep = "\t", col.names = FALSE, row.names = FALSE, eol = "\n\n")

# Save pbmc object
saveRDS(pbmc, file = paste(temp.dir, "/pbmc_with_clusters.rds", sep = ""))
