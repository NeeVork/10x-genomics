##################################################
## Project: CLAP tenX
## Script purpose: Make barplot of frequency of clonotypes in csv file
## Date: 2020-16-01
## Author: Ewoud, Naomi
##################################################


# setwd("D:\\MEGA\\School\\Themas\\jaar2\\thema06\\project\\10x-genomics")

args <- commandArgs(trailingOnly = TRUE)

# Set variables form command line
temp.dir <- args[1]
file.name <- args[2]
amount.clonotypes <- as.integer(args[3])

# Read csv file
db = read.csv(file.name, header=TRUE)

# Add suffic to double barcodes
db$barcode <- make.unique(as.character(db$barcode), sep = "_")

# Filter data
filtered.data <- subset(
    x = db, 
    subset = raw_clonotype_id != 'NA' &
             raw_clonotype_id != 'None' &
             !is.na(raw_clonotype_id) &
             is_cell == "True" &
             nchar(barcode) == 18
                        
  )
                        
clonotypes <- filtered.data$raw_clonotype_id

# Make a dataframe from clonotypes (count how often every clonotype occurs)
my.df <- as.data.frame(table(clonotypes))

# Order dataframe from most abundant clonotype to least abundant
my.df <- my.df[order(my.df$Freq, my.df$clonotypes, decreasing = TRUE),]

# Save image of barplot
png(paste(temp.dir, "/freq_clonotypes.png", sep = ""), width = 600, height = 600)
if (amount.clonotypes <= 15) {
  barplot(
      head(my.df, amount.clonotypes)$Freq, xlab="clonotype", 
      ylab="frequency", 
      names.arg = substr(head(my.df$clonotypes, amount.clonotypes), 10, 12),
      col = "#D77D71",
      main = "Abundancy largest clonotypes contig file"
    )
} else if (amount.clonotypes > length(my.df$clonotypes)) {
  barplot(
      my.df$Freq, 
      xlab="clonotype", 
      ylab="frequency",
      col = "#D77D71",
      main = "Abundancy largest clonotypes contig file"
    )
} else {
  barplot(
      head(my.df, amount.clonotypes)$Freq, 
      xlab="clonotype", 
      ylab="frequency",
      col = "#D77D71",
      main = "Abundancy largest clonotypes contig file"
    )
}
dev.off() 

# Save R objects in temp.dir
saveRDS(
    filtered.data, 
    file = paste(temp.dir, "/contig_filtered_data.rds", sep = "")
  )
saveRDS(
    my.df, 
    file = paste(temp.dir, "/clonotype_freq.rds", sep = "")
  )
