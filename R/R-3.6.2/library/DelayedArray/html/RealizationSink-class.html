<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: RealizationSink objects</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for RealizationSink {DelayedArray}"><tr><td>RealizationSink {DelayedArray}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>RealizationSink objects</h2>

<h3>Description</h3>

<p>Get or set the <em>realization backend</em> for the current session with
<code>getRealizationBackend</code> or <code>setRealizationBackend</code>.
</p>
<p>Advanced users: Create a RealizationSink object with the backend-agnostic
<code>RealizationSink()</code> constructor. Use this object to write an
array-like object block by block to disk (or to memory).
</p>


<h3>Usage</h3>

<pre>
supportedRealizationBackends()
getRealizationBackend()
setRealizationBackend(BACKEND=NULL)

RealizationSink(dim, dimnames=NULL, type="double")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>BACKEND</code></td>
<td>

<p><code>NULL</code> (the default), or a single string specifying the name of
a <em>realization backend</em>.
</p>
</td></tr>
<tr valign="top"><td><code>dim</code></td>
<td>

<p>The dimensions (specified as an integer vector) of the RealizationSink
object to create.
</p>
</td></tr>
<tr valign="top"><td><code>dimnames</code></td>
<td>

<p>The dimnames (specified as a list of character vectors or NULLs) of
the RealizationSink object to create.
</p>
</td></tr>
<tr valign="top"><td><code>type</code></td>
<td>

<p>The type of the data that will be written to the RealizationSink
object to create.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The <em>realization backend</em> controls where/how realization happens e.g.
as an ordinary array if set to <code>NULL</code>, as an <a href="RleArray-class.html">RleArray</a> object
if set to <code>"RleArray"</code>, or as an <a href="../../HDF5Array/html/HDF5Array.html">HDF5Array</a> object
if set to <code>"HDF5Array"</code>.
</p>


<h3>Value</h3>

<p><code>supportedRealizationBackends</code>: A data frame with 1 row per
supported <em>realization backend</em>.
</p>
<p><code>getRealizationBackend</code>: <code>NULL</code> or a single string specifying
the name of the <em>realization backend</em> currently in use.
</p>
<p><code>RealizationSink</code>: A RealizationSink object for the current
<em>realization backend</em>.
</p>


<h3>See Also</h3>


<ul>
<li> <p><code><a href="read_block.html">write_block</a></code>.
</p>
</li>
<li> <p><code><a href="blockGrid.html">blockGrid</a></code> to define grids to use in the context
of block processing of array-like objects.
</p>
</li>
<li> <p><a href="DelayedArray-class.html">DelayedArray</a> objects.
</p>
</li>
<li> <p><a href="RleArray-class.html">RleArray</a> objects.
</p>
</li>
<li> <p><a href="../../HDF5Array/html/HDF5Array.html">HDF5Array</a> objects in the <span class="pkg">HDF5Array</span> package.
</p>
</li>
<li> <p><a href="../../HDF5Array/html/HDF5-dump-management.html">HDF5-dump-management</a> in the <span class="pkg">HDF5Array</span>
package to control the location and physical properties of
automatically created HDF5 datasets.
</p>
</li>
<li> <p><a href="../../base/html/array.html">array</a> objects in base R.
</p>
</li></ul>



<h3>Examples</h3>

<pre>
## ---------------------------------------------------------------------
## A. supportedRealizationBackends() AND FAMILY
## ---------------------------------------------------------------------
supportedRealizationBackends()
getRealizationBackend()  # backend is set to NULL

setRealizationBackend("HDF5Array")
supportedRealizationBackends()
getRealizationBackend()  # backend is set to "HDF5Array"

## ---------------------------------------------------------------------
## B. A SIMPLE (AND VERY ARTIFICIAL) RealizationSink() EXAMPLE
## ---------------------------------------------------------------------
getHDF5DumpChunkLength()
setHDF5DumpChunkLength(500L)
getHDF5DumpChunkShape()

sink &lt;- RealizationSink(c(120L, 50L))
dim(sink)
chunkdim(sink)

grid &lt;- blockGrid(sink, block.length=600)
for (b in seq_along(grid)) {
    viewport &lt;- grid[[b]]
    block &lt;- 101 * b + runif(length(viewport))
    dim(block) &lt;- dim(viewport)
    write_block(sink, viewport, block)
}

## Always close the RealizationSink object when you are done writing to
## it and before coercing it to DelayedArray:
close(sink)
A &lt;- as(sink, "DelayedArray")
A

## ---------------------------------------------------------------------
## C. AN ADVANCED EXAMPLE OF USER-IMPLEMENTED BLOCK PROCESSING USING
##    colGrid() AND A REALIZATION SINK
## ---------------------------------------------------------------------
## Say we have 2 matrices with the same number of columns. Each column
## represents a biological sample:
library(HDF5Array)
R &lt;- as(matrix(runif(75000), ncol=1000), "HDF5Array")   # 75 rows
G &lt;- as(matrix(runif(250000), ncol=1000), "HDF5Array")  # 250 rows

## Say we want to compute the matrix U obtained by applying the same
## binary functions FUN() to all samples i.e. U is defined as:
##
##   U[ , j] &lt;- FUN(R[ , j], G[ , j]) for 1 &lt;= j &lt;= 1000
##
## Note that FUN() should return a vector of constant length, say 200,
## so U will be a 200x1000 matrix. A naive implementation would be:
##
##   pFUN &lt;- function(r, g) {
##       stopifnot(ncol(r) == ncol(g))  # sanity check
##       sapply(seq_len(ncol(r)), function(j) FUN(r[ , j], g[ , j]))
##   }
##
## But because U is going to be too big to fit in memory, we can't
## just do pFUN(R, G). So we want to compute U block by block and
## write the blocks to disk as we go. The blocks will be made of full
## columns. Also since we need to walk on 2 matrices at the same time
## (R and G), we can't use blockApply() or blockReduce() so we'll use
## a "for" loop.

## Before we can write the "for" loop, we need 4 things:

## 1) Two grids of blocks, one on R and one on G. The blocks in the
##    2 grids must contain the same number of columns. We arbitrarily
##    choose to use blocks of 150 columns:
R_grid &lt;- colGrid(R, ncol=150)
G_grid &lt;- colGrid(G, ncol=150)

## 2) The function pFUN(). It will take 2 blocks as input, 1 from R
##    and 1 from G, apply FUN() to all the samples in the blocks,
##    and return a matrix with one columns per sample:
pFUN &lt;- function(r, g) {
    stopifnot(ncol(r) == ncol(g))  # sanity check
    ## Return a matrix with 200 rows with random values. Completely
    ## artificial sorry. A realistic example would actually need to
    ## apply the same binary function to r[ ,j] and g[ , j] for
    ## 1 &lt;= j &lt;= ncol(r).
    matrix(runif(200 * ncol(r)), nrow=200)
}

## 3) A RealizationSink object where to write the matrices returned
##    by pFUN() as we go. Note that instead of creating a realization
##    sink by calling a backend-specific sink constructor (e.g.
##    HDF5Array:::HDF5RealizationSink), we use the backend-agnostic
##    constructor RealizationSink() and set the current realization
##    backend to HDF5:

setRealizationBackend("HDF5Array")
U_sink &lt;- RealizationSink(c(200L, 1000L))

## 4) Finally, we create a grid on U_sink with blocks that contain the
##    same number of columns as the corresponding blocks in R and G:

U_grid &lt;- colGrid(U_sink, ncol=150)

## Note that the 3 grids should have the same number of blocks:
stopifnot(length(U_grid) == length(R_grid))
stopifnot(length(U_grid) == length(G_grid))

## Now we can procede. We write a loop where we walk on R and G at
## the same time, block by block, apply pFUN(), and write the output
## of pFUN() to U_sink:
for (b in seq_along(U_grid)) {
    R_block &lt;- read_block(R, R_grid[[b]])
    G_block &lt;- read_block(G, G_grid[[b]])
    U_block &lt;- pFUN(R_block, G_block)
    write_block(U_sink, U_grid[[b]], U_block)
}

close(U_sink)
U &lt;- as(U_sink, "DelayedArray")

## A note about parallelization: even though concurrent block reading
## from the same object is supported, concurrent writing to a sink is
## not supported yet. So the above code cannot be parallelized at the
## moment.
</pre>

<hr /><div style="text-align: center;">[Package <em>DelayedArray</em> version 0.12.0 <a href="00Index.html">Index</a>]</div>
</body></html>
