<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Define grids to use in the context of block processing</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for blockGrid {DelayedArray}"><tr><td>blockGrid {DelayedArray}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Define grids to use in the context of block processing</h2>

<h3>Description</h3>

<p><code>blockGrid()</code> is the primary utility function to use to define a grid
that is suitable for block processing of an array-like object.
</p>
<p><code>rowGrid()</code> and <code>colGrid()</code> are additional functions, specific
to the 2-dimensional case. They can be used to define blocks of full rows
or full columns.
</p>
<p>A family of utilities is provided to control the automatic block size (or
length) and shape.
</p>


<h3>Usage</h3>

<pre>
## Define grids to use in the context of block processing:

blockGrid(x, block.length=NULL, chunk.grid=NULL, block.shape=NULL)

rowGrid(x, nrow=NULL, block.length=NULL)
colGrid(x, ncol=NULL, block.length=NULL)

## Control the automatic block size (or length) and shape:

getAutoBlockSize()
setAutoBlockSize(size=1e8)

getAutoBlockLength(type)

getAutoBlockShape()
setAutoBlockShape(shape=c("hypercube",
                          "scale",
                          "first-dim-grows-first",
                          "last-dim-grows-first"))
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>x</code></td>
<td>

<p>An array-like or matrix-like object for <code>blockGrid</code>.
</p>
<p>A matrix-like object for <code>rowGrid</code> and <code>colGrid</code>.
</p>
</td></tr>
<tr valign="top"><td><code>block.length</code></td>
<td>

<p>The length of the blocks i.e. the number of array elements per block.
By default the automatic block length (returned by
<code>getAutoBlockLength(type(x))</code>) is used.
Depending on how much memory is available on your machine, you might
want to increase (or decrease) the automatic block length by adjusting
the automatic block size with <code>setAutoBlockSize()</code>.
</p>
</td></tr>
<tr valign="top"><td><code>chunk.grid</code></td>
<td>

<p>The grid of physical chunks.
By default <code><a href="chunkGrid.html">chunkGrid</a>(x)</code> is used.
</p>
</td></tr>
<tr valign="top"><td><code>block.shape</code></td>
<td>

<p>A string specifying the shape of the blocks.
See <code><a href="makeCappedVolumeBox.html">makeCappedVolumeBox</a></code> for a description of the
supported shapes.
By default <code>getAutoBlockShape()</code> is used.
</p>
</td></tr>
<tr valign="top"><td><code>nrow</code></td>
<td>

<p>The number of rows of the blocks. The bottommost blocks might have less.
See examples below.
</p>
</td></tr>
<tr valign="top"><td><code>ncol</code></td>
<td>

<p>The number of columns of the blocks. The rightmost blocks might have less.
See examples below.
</p>
</td></tr>
<tr valign="top"><td><code>size</code></td>
<td>

<p>The automatic block size in bytes. Note that, except when the type of
the array data is <code>"character"</code> or <code>"list"</code>, the size of a
block is its length multiplied by the size of an array element.
For example, a block of 500x1000x500 doubles has a length of 250 million
elements and a size of 2 Gb (each double occupies 8 bytes of memory).
</p>
<p>The automatic block size is set to 100 Mb at package startup and can
be reset anytime to this value by calling <code>setAutoBlockSize()</code>
with no argument.
</p>
</td></tr>
<tr valign="top"><td><code>type</code></td>
<td>

<p>A string specifying the type of the array data.
</p>
</td></tr>
<tr valign="top"><td><code>shape</code></td>
<td>

<p>A string specifying the automatic block shape.
See <code><a href="makeCappedVolumeBox.html">makeCappedVolumeBox</a></code> for a description of the
supported shapes.
</p>
<p>The automatic block shape is set to <code>"hypercube"</code> at package
startup and can be reset anytime to this value by calling
<code>setAutoBlockShape()</code> with no argument.
</p>
</td></tr>
</table>


<h3>Details</h3>

<p>By default, primary block processing functions <code><a href="block_processing.html">blockApply</a>()</code>
and <code><a href="block_processing.html">blockReduce</a>()</code> use the grid returned by <code>blockGrid(x)</code>
to process array-like object <code>x</code> block by block. This can be changed
with <code><a href="block_processing.html">setAutoGridMaker</a>()</code>.
See <code>?<a href="block_processing.html">setAutoGridMaker</a></code> for more information.
</p>


<h3>Value</h3>

<p><code>blockGrid</code>: An <a href="ArrayGrid-class.html">ArrayGrid</a> object on reference array <code>x</code>.
The grid elements define the blocks that will be used to process <code>x</code>
by block. The grid is <em>optimal</em> in the sense that:
</p>

<ol>
<li><p> It's <em>compatible</em> with the grid of physical chunks a.k.a.
<em>chunk grid</em>. This means that, when the chunk grid is known
(i.e. when <code><a href="chunkGrid.html">chunkGrid</a>(x)</code> is not NULL or
<code>chunk.grid</code> is supplied), every block in the grid contains
one or more <em>full</em> chunks. In other words, chunks never cross
block boundaries.
</p>
</li>
<li><p> Its <em>resolution</em> is such that the blocks have a length
that is as close as possibe to (but does not exceed)
<code>block.length</code>. An exception is made when some chunks
already have a length that is &gt;= <code>block.length</code>, in which
case the returned grid is the same as the chunk grid.
</p>
</li></ol>

<p>Note that the returned grid is regular (i.e. is a <a href="ArrayGrid-class.html">RegularArrayGrid</a>
object) unless the chunk grid is not regular (i.e. is an
<a href="ArrayGrid-class.html">ArbitraryArrayGrid</a> object).
</p>
<p><code>rowGrid</code>: A <a href="ArrayGrid-class.html">RegularArrayGrid</a> object on reference array <code>x</code>
where the grid elements define blocks made of full rows of <code>x</code>.
</p>
<p><code>colGrid</code>: A <a href="ArrayGrid-class.html">RegularArrayGrid</a> object on reference array <code>x</code>
where the grid elements define blocks made of full columns of <code>x</code>.
</p>
<p><code>getAutoBlockSize</code>: The current automatic block size in bytes
as a single numeric value.
</p>
<p><code>setAutoBlockSize</code>: The new automatic block size in bytes as an
invisible single numeric value.
</p>
<p><code>getAutoBlockLength</code>: The automatic block length as a single integer
value.
</p>
<p><code>getAutoBlockShape</code>: The current automatic block shape as a
single string.
</p>
<p><code>setAutoBlockShape</code>: The new automatic block shape as an invisible
single string.
</p>


<h3>See Also</h3>


<ul>
<li> <p><code><a href="block_processing.html">blockApply</a></code> and family to process an array-like
object block by block.
</p>
</li>
<li> <p><a href="ArrayGrid-class.html">ArrayGrid</a> objects.
</p>
</li>
<li><p> The <code><a href="makeCappedVolumeBox.html">makeCappedVolumeBox</a></code> utility to make
<em>capped volume boxes</em>.
</p>
</li>
<li> <p><code><a href="chunkGrid.html">chunkGrid</a></code>.
</p>
</li>
<li><p> Advanced users: <a href="RealizationSink-class.html">RealizationSink</a> objects for writing an
array-like object block by block to disk (or to memory).
</p>
</li></ul>



<h3>Examples</h3>

<pre>
## ---------------------------------------------------------------------
## A VERSION OF sum() THAT USES BLOCK PROCESSING
## ---------------------------------------------------------------------

block_sum &lt;- function(a, grid)
{
    sums &lt;- lapply(grid, function(viewport) sum(read_block(a, viewport)))
    sum(unlist(sums))
}

## On an ordinary matrix:
m &lt;- matrix(runif(600), ncol=12)
m_grid &lt;- blockGrid(m, block.length=120)
sum1 &lt;- block_sum(m, m_grid)
sum1

## On a DelayedArray object:
library(HDF5Array)
M &lt;- as(m, "HDF5Array")
sum2 &lt;- block_sum(M, m_grid)
sum2

sum3 &lt;- block_sum(M, colGrid(M, block.length=120))
sum3

sum4 &lt;- block_sum(M, rowGrid(M, block.length=80))
sum4

## Sanity checks:
sum0 &lt;- sum(m)
stopifnot(identical(sum1, sum0))
stopifnot(identical(sum2, sum0))
stopifnot(identical(sum3, sum0))
stopifnot(identical(sum4, sum0))

## ---------------------------------------------------------------------
## blockGrid()
## ---------------------------------------------------------------------
grid &lt;- blockGrid(m, block.length=120)
grid
as.list(grid)  # turn the grid into a list of ArrayViewport objects
table(lengths(grid))
stopifnot(maxlength(grid) &lt;= 120)

grid &lt;- blockGrid(m, block.length=120,
                     block.shape="first-dim-grows-first")
grid
table(lengths(grid))
stopifnot(maxlength(grid) &lt;= 120)

grid &lt;- blockGrid(m, block.length=120,
                     block.shape="last-dim-grows-first")
grid
table(lengths(grid))
stopifnot(maxlength(grid) &lt;= 120)

blockGrid(m, block.length=100)
blockGrid(m, block.length=75)
blockGrid(m, block.length=25)
blockGrid(m, block.length=20)
blockGrid(m, block.length=10)

## ---------------------------------------------------------------------
## rowGrid() AND colGrid()
## ---------------------------------------------------------------------
rowGrid(m, nrow=10)  # 5 blocks of 10 rows each
rowGrid(m, nrow=15)  # 3 blocks of 15 rows each plus 1 block of 5 rows
colGrid(m, ncol=5)   # 2 blocks of 5 cols each plus 1 block of 2 cols

## See ?RealizationSink for an advanced example of user-implemented
## block processing using colGrid() and a realization sink.

## ---------------------------------------------------------------------
## CONTROL THE DEFAULT BLOCK SIZE (OR LENGTH) AND SHAPE
## ---------------------------------------------------------------------
getAutoBlockSize()

getAutoBlockLength("double")
getAutoBlockLength("integer")
getAutoBlockLength("logical")
getAutoBlockLength("raw")

setAutoBlockSize(140)
getAutoBlockLength(type(m))
blockGrid(m)
lengths(blockGrid(m))
dims(blockGrid(m))

getAutoBlockShape()
setAutoBlockShape("scale")
blockGrid(m)
lengths(blockGrid(m))
dims(blockGrid(m))

## Reset automatic block size and shape to factory settings:
setAutoBlockSize()
setAutoBlockShape()
</pre>

<hr /><div style="text-align: center;">[Package <em>DelayedArray</em> version 0.12.0 <a href="00Index.html">Index</a>]</div>
</body></html>
