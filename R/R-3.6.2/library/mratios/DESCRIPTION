Package: mratios
Type: Package
Title: Ratios of Coefficients in the General Linear Model
Version: 1.4.0
Date: 2018-05-23
Authors@R: c(person("Gemechis", "Djira", role="aut"),
 person("Mario","Hasler", role="aut"),
 person("Daniel", "Gerhard", role="aut"),
 person("Frank","Schaarschmidt", role=c("aut","cre"), email="schaarschmidt@biostat.uni-hannover.de"))
Author: Gemechis Djira [aut],
  Mario Hasler [aut],
  Daniel Gerhard [aut],
  Frank Schaarschmidt [aut, cre]
Maintainer: Frank Schaarschmidt <schaarschmidt@biostat.uni-hannover.de>
Depends: R (>= 2.12.0)
Suggests: multcomp, nlme
Imports: mvtnorm, stats
Description: Performs (simultaneous) inferences for ratios of linear combinations of coefficients in the general linear model. Multiple comparisons and simultaneous confidence interval estimations can be performed for ratios of treatment means in the normal one-way layout with homogeneous and heterogeneous treatment variances, according to Dilba et al. (2007) <https://cran.r-project.org/doc/Rnews/Rnews_2007-1.pdf> and Hasler and Hothorn (2008) <doi:10.1002/bimj.200710466>. Confidence interval estimations for ratios of linear combinations of linear model parameters like in (multiple) slope ratio and parallel line assays can be carried out. Moreover, it is possible to calculate the sample sizes required in comparisons with a control based on relative margins. For the simple two-sample problem, functions for a t-test for ratio-formatted hypotheses and the corresponding confidence interval are provided assuming homogeneous or heterogeneous group variances.
License: GPL-2
NeedsCompilation: no
Packaged: 2018-05-23 14:53:50 UTC; Schaarschmidt
Repository: CRAN
Date/Publication: 2018-05-23 15:51:55 UTC
Built: R 3.6.1; ; 2019-12-12 18:37:40 UTC; windows
