<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: 9. Advanced - Cell-index maps for reading and writing</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for 9. Advanced - Cell-index maps for reading and writing {affxparser}"><tr><td>9. Advanced - Cell-index maps for reading and writing {affxparser}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>9. Advanced - Cell-index maps for reading and writing</h2>

<h3>Description</h3>

<p>This part defines read and write maps that can be used to remap
cell indices before reading and writing data from and to file,
respectively.
</p>
<p>This package provides methods to create read and write (cell-index)
maps from Affymetrix CDF files.  These can be used to store the cell
data in an optimal order so that when data is read it is read in
contiguous blocks, which is faster.
</p>
<p>In addition to this, read maps may also be used to read CEL files that
have been &quot;reshuffled&quot; by other software.  For instance, the dChip
software (<a href="http://www.dchip.org/">http://www.dchip.org/</a>) rotates Affymetrix Exon,
Tiling and Mapping 500K data.  See example below how to read
such data &quot;unrotated&quot;.
</p>
<p>For more details how cell indices are defined, see
<code><a href="2._Cell_coordinates_and_cell_indices.html">2. Cell coordinates and cell indices</a></code>.
</p>


<h3>Motivation</h3>

<p>When reading data from file, it is faster to read the data in
the order that it is stored compared with, say, in a random order.
The main reason for this is that the read arm of the hard drive
has to move more if data is not read consecutively.  Same applies
when writing data to file.  The read and write cache of the file
system may compensate a bit for this, but not completely.
</p>
<p>In Affymetrix CEL files, cell data is stored in order of cell indices.
Moreover, (except for a few early chip types) Affymetrix randomizes
the locations of the cells such that cells in the same unit (probeset)
are scattered across the array.
Thus, when reading CEL data arranged by units using for instance
<code><a href="readCelUnits.html">readCelUnits</a></code>(), the order of the cells requested is both random
and scattered.
</p>
<p>Since CEL data is often queried unit by unit (except for some
probe-level normalization methods), one can improve the speed of
reading data by saving data such that cells in the same unit are
stored together.  A <em>write map</em> is used to remap cell indices
to file indices.  When later reading that data back, a
<em>read map</em> is used to remap file indices to cell indices.
Read and write maps are described next.
</p>


<h3>Definition of read and write maps</h3>

<p>Consider cell indices <i>i=1, 2, ..., N*K</i> and file indices
<i>j=1, 2, ..., N*K</i>.
A <em>read map</em> is then a <em>bijective</em> (one-to-one) function
<i>h()</i> such that
</p>
<p style="text-align: center;"><i>
     i = h(j),
   </i></p>

<p>and the corresponding <em>write map</em> is the inverse function
<i>h^{-1}()</i> such that
</p>
<p style="text-align: center;"><i>
     j = h^{-1}(i).
   </i></p>

<p>Since the mapping is required to be bijective, it holds that
<i>i = h(h^{-1}(i))</i> and that <i>j = h^{-1}(h(j))</i>.
For example, consider the &quot;reversing&quot; read map function
<i>h(j)=N*K-j+1</i>.  The write map function is <i>h^{-1}(i)=N*K-i+1</i>.
To verify the bijective property of this map, we see that
<i>h(h^{-1}(i)) = h(N*K-i+1) = N*K-(N*K-i+1)+1 = i</i> as well as
<i>h^{-1}(h(j)) = h^{-1}(N*K-j+1) = N*K-(N*K-j+1)+1 = j</i>.
</p>


<h3>Read and write maps in R</h3>

<p>In this package, read and write maps are represented as <code><a href="../../base/html/integer.html">integer</a></code>
<code><a href="../../base/html/vector.html">vector</a></code>s of length <i>N*K</i> with <em>unique</em> elements in
<i>\{1,2,...,N*K\}</i>.
Consider cell and file indices as in previous section.
</p>
<p>For example, the &quot;reversing&quot; read map in previous section can be
represented as
</p>
<pre>
     readMap &lt;- (N*K):1
   </pre>
<p>Given a <code><a href="../../base/html/vector.html">vector</a></code> <code>j</code> of file indices, the cell indices are
the obtained as <code>i = readMap[j]</code>.
The corresponding write map is
</p>
<pre>
     writeMap &lt;- (N*K):1
   </pre>
<p>and given a <code><a href="../../base/html/vector.html">vector</a></code> <code>i</code> of cell indices, the file indices are
the obtained as <code>j = writeMap[i]</code>.
</p>
<p>Note also that the bijective property holds for this mapping, that is
<code>i == readMap[writeMap[i]]</code> and <code>i == writeMap[readMap[i]]</code>
are both <code><a href="../../base/html/logical.html">TRUE</a></code>.
</p>
<p>Because the mapping is bijective, the write map can be calculated from
the read map by:
</p>
<pre>
     writeMap &lt;- order(readMap)
   </pre>
<p>and vice versa:
</p>
<pre>
     readMap &lt;- order(writeMap)
   </pre>
<p>Note, the <code><a href="invertMap.html">invertMap</a></code>() method is much faster than <code>order()</code>.
</p>
<p>Since most algorithms for Affymetrix data are based on probeset (unit)
models, it is natural to read data unit by unit.  Thus, to optimize the
speed, cells should be stored in contiguous blocks of units.
The methods <code><a href="readCdfUnitsWriteMap.html">readCdfUnitsWriteMap</a></code>() can be used to generate a
<em>write map</em> from a CDF file such that if the units are read in
order, <code><a href="readCelUnits.html">readCelUnits</a></code>() will read the cells data in order.
Example:
</p>
<pre>
     Find any CDF file
     cdfFile &lt;- findCdf()

     # Get the order of cell indices
     indices &lt;- readCdfCellIndices(cdfFile)
     indices &lt;- unlist(indices, use.names=FALSE)

     # Get an optimal write map for the CDF file
     writeMap &lt;- readCdfUnitsWriteMap(cdfFile)

     # Get the read map
     readMap &lt;- invertMap(writeMap)

     # Validate correctness
     indices2 &lt;- readMap[indices]    # == 1, 2, 3, ..., N*K
   </pre>
<p><em>Warning</em>, do not misunderstand this example.  It can not be used
improve the reading speed of default CEL files.  For this, the data in
the CEL files has to be rearranged (by the corresponding write map).
</p>


<h3>Reading rotated CEL files</h3>

<p>It might be that a CEL file was rotated by another software, e.g.
the dChip software rotates Affymetrix Exon, Tiling and Mapping 500K
arrays 90 degrees clockwise, which remains rotated when exported
as CEL files.  To read such data in a non-rotated way, a read
map can be used to &quot;unrotate&quot; the data.  The 90-degree clockwise
rotation that dChip effectively uses to store such data is explained by:
</p>
<pre>
     h &lt;- readCdfHeader(cdfFile)
     # (x,y) chip layout rotated 90 degrees clockwise
     nrow &lt;- h$cols
     ncol &lt;- h$rows
     y &lt;- (nrow-1):0
     x &lt;- rep(1:ncol, each=nrow)
     writeMap &lt;- as.vector(y*ncol + x)
   </pre>
<p>Thus, to read this data &quot;unrotated&quot;, use the following read map:
</p>
<pre>
     readMap &lt;- invertMap(writeMap)
     data &lt;- readCel(celFile, indices=1:10, readMap=readMap)
   </pre>


<h3>Author(s)</h3>

<p>Henrik Bengtsson</p>

<hr /><div style="text-align: center;">[Package <em>affxparser</em> version 1.58.0 <a href="00Index.html">Index</a>]</div>
</body></html>
