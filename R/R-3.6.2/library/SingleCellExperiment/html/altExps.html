<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Alternative Experiment methods</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for altExps {SingleCellExperiment}"><tr><td>altExps {SingleCellExperiment}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Alternative Experiment methods</h2>

<h3>Description</h3>

<p>In some experiments, different features must be normalized differently or have different row-level metadata.
Typical examples would be for spike-in transcripts in plate-based experiments and antibody or CRISPR tags in CITE-seq experiments.
These data cannot be stored in the main <code>assays</code> of the <a href="SingleCellExperiment.html">SingleCellExperiment</a> itself.
However, it is still desirable to store these features <em>somewhere</em> in the SingleCellExperiment.
This simplifies book-keeping in long workflows and ensure that samples remain synchronised.
</p>
<p>To facilitate this, the <a href="SingleCellExperiment.html">SingleCellExperiment</a> class allows for &ldquo;alternative Experiments&rdquo;.
Nested <a href="../../SummarizedExperiment/html/SummarizedExperiment-class.html">SummarizedExperiment</a>-class objects are stored inside the SingleCellExperiment object <code>x</code>, in a manner that guarantees that the nested objects have the same columns in the same order as those in <code>x</code>.
Methods are provided to enable convenient access to and manipulation of these alternative Experiments.
Each alternative Experiment should contain experimental data and row metadata for a distinct set of features.
</p>


<h3>Getters</h3>

<p>In the following examples, <code>x</code> is a <a href="SingleCellExperiment.html">SingleCellExperiment</a> object.
</p>

<dl>
<dt><code>altExp(x, e, withColData=TRUE)</code>:</dt><dd>
<p>Retrieves a <a href="../../SummarizedExperiment/html/SummarizedExperiment-class.html">SummarizedExperiment</a> containing alternative features (rows) for all cells (columns) in <code>x</code>.
<code>e</code> is either a string specifying the name of the alternative Experiment in <code>x</code> to retrieve,
or a numeric scalar specifying the index of the desired Experiment.
If <code>withColData=TRUE</code>, the column metadata of the output object is set to <code><a href="../../GenoGAM/html/GenoGAM-class.html">colData</a>(x)</code>.
</p>
</dd>
<dt><code>altExpNames(x)</code>:</dt><dd>
<p>Returns a character vector containing the names of all alternative Experiments in <code>x</code>.
This is guaranteed to be of the same length as the number of results, though the names may not be unique.
</p>
</dd>
<dt><code>altExps(x, withColData=TRUE)</code>:</dt><dd>
<p>Returns a named <a href="../../S4Vectors/html/List-class.html">List</a> of matrices containing one or more <a href="../../SummarizedExperiment/html/SummarizedExperiment-class.html">SummarizedExperiment</a> objects.
Each object has the same number of columns.
If <code>withColData=TRUE</code>, the column metadata of each output object is set to <code><a href="../../GenoGAM/html/GenoGAM-class.html">colData</a>(x)</code>.
</p>
</dd>
</dl>



<h3>Single-object setter</h3>

<p><code>altExp(x, e) &lt;- value</code> will add or replace an alternative Experiment
in a <a href="SingleCellExperiment.html">SingleCellExperiment</a> object <code>x</code>.
The value of <code>e</code> determines how the result is added or replaced:
</p>

<ul>
<li><p> If <code>e</code> is missing, <code>value</code> is assigned to the first result.
If the result already exists, its name is preserved; otherwise it is given a default name <code>"unnamed1"</code>.
</p>
</li>
<li><p> If <code>e</code> is a numeric scalar, it must be within the range of existing results, and <code>value</code> will be assigned to the result at that index.
</p>
</li>
<li><p> If <code>e</code> is a string and a result exists with this name, <code>value</code> is assigned to to that result.
Otherwise a new result with this name is append to the existing list of results.
</p>
</li></ul>

<p><code>value</code> is expected to be a SummarizedExperiment object with number of columns equal to <code>ncol(x)</code>.
Alternatively, if <code>value</code> is <code>NULL</code>, the alternative Experiment at <code>e</code> is removed from the object.
</p>


<h3>Other setters</h3>

<p>In the following examples, <code>x</code> is a <a href="SingleCellExperiment.html">SingleCellExperiment</a> object.
</p>

<dl>
<dt><code>altExps(x) &lt;- value</code>:</dt><dd>
<p>Replaces all alterrnative Experiments in <code>x</code> with those in <code>value</code>.
The latter should be a list-like object containing any number of SummarizedExperiment objects
with number of columns equal to <code>ncol(x)</code>.
</p>
<p>If <code>value</code> is named, those names will be used to name the alternative Experiments in <code>x</code>.
Otherwise, unnamed results are assigned default names prefixed with <code>"unnamed"</code>.
</p>
<p>If <code>value</code> is <code>NULL</code>, all alternative Experiments in <code>x</code> are removed.
</p>
</dd>
<dt><code>altExpNames(x) &lt;- value</code>:</dt><dd>
<p>Replaces all names for alternative Experiments in <code>x</code> with a character vector <code>value</code>.
This should be of length equal to the number of results currently in <code>x</code>.
</p>
</dd>
</dl>

<p><code>removeAltExps(x)</code> will remove all alternative Experiments from <code>x</code>.
This has the same effect as <code>altExps(x) &lt;- NULL</code> but may be more convenient as it directly returns a SingleCellExperiment.
</p>


<h3>Author(s)</h3>

<p>Aaron Lun
</p>


<h3>See Also</h3>

<p><code><a href="splitAltExps.html">splitAltExps</a></code>, for a convenient way of adding alternative Experiments from existing features.
</p>
<p><code><a href="swapAltExp.html">swapAltExp</a></code>, to swap the main and alternative Experiments.
</p>


<h3>Examples</h3>

<pre>
example(SingleCellExperiment, echo=FALSE) # Using the class example
dim(counts(sce))

# Mocking up some alternative Experiments.
se1 &lt;- SummarizedExperiment(matrix(rpois(1000, 5), ncol=ncol(se)))
rowData(se1)$stuff &lt;- sample(LETTERS, nrow(se1), replace=TRUE)
se2 &lt;- SummarizedExperiment(matrix(rpois(500, 5), ncol=ncol(se)))
rowData(se2)$blah &lt;- sample(letters, nrow(se2), replace=TRUE)

# Setting the alternative Experiments.
altExp(sce, "spike-in") &lt;- se1
altExp(sce, "CRISPR") &lt;- se2

# Getting alternative Experimental data.
altExpNames(sce)
altExp(sce, "spike-in")
altExp(sce, 2)

# Setting alternative Experimental data.
altExpNames(sce) &lt;- c("ERCC", "Ab")
altExp(sce, "ERCC") &lt;- se1[1:2,]

</pre>

<hr /><div style="text-align: center;">[Package <em>SingleCellExperiment</em> version 1.8.0 <a href="00Index.html">Index</a>]</div>
</body></html>
