<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Apply a variance stabilizing transformation (VST) to the...</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for varianceStabilizingTransformation {DESeq2}"><tr><td>varianceStabilizingTransformation {DESeq2}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Apply a variance stabilizing transformation (VST) to the count data</h2>

<h3>Description</h3>

<p>This function calculates a variance stabilizing transformation (VST) from the
fitted dispersion-mean relation(s) and then transforms the count data (normalized
by division by the size factors or normalization factors), yielding a matrix
of values which are now approximately homoskedastic (having constant variance along the range
of mean values). The transformation also normalizes with respect to library size.
The <code><a href="rlog.html">rlog</a></code> is less sensitive
to size factors, which can be an issue when size factors vary widely.
These transformations are useful when checking for outliers or as input for
machine learning techniques such as clustering or linear discriminant analysis.
</p>


<h3>Usage</h3>

<pre>
varianceStabilizingTransformation(object, blind = TRUE,
  fitType = "parametric")

getVarianceStabilizedData(object)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>
<p>a DESeqDataSet or matrix of counts</p>
</td></tr>
<tr valign="top"><td><code>blind</code></td>
<td>
<p>logical, whether to blind the transformation to the experimental
design. blind=TRUE should be used for comparing samples in an manner unbiased by
prior information on samples, for example to perform sample QA (quality assurance).
blind=FALSE should be used for transforming data for downstream analysis,
where the full use of the design information should be made.
blind=FALSE will skip re-estimation of the dispersion trend, if this has already been calculated.
If many of genes have large differences in counts due to
the experimental design, it is important to set blind=FALSE for downstream
analysis.</p>
</td></tr>
<tr valign="top"><td><code>fitType</code></td>
<td>
<p>in case dispersions have not yet been estimated for <code>object</code>,
this parameter is passed on to <code><a href="estimateDispersions.html">estimateDispersions</a></code> (options described there).</p>
</td></tr>
</table>


<h3>Details</h3>

<p>For each sample (i.e., column of <code>counts(dds)</code>), the full variance function
is calculated from the raw variance (by scaling according to the size factor and adding 
the shot noise). We recommend a blind estimation of the variance function, i.e.,
one ignoring conditions. This is performed by default, and can be modified using the
'blind' argument.
</p>
<p>Note that neither rlog transformation nor the VST are used by the
differential expression estimation in <code><a href="DESeq.html">DESeq</a></code>, which always
occurs on the raw count data, through generalized linear modeling which
incorporates knowledge of the variance-mean dependence. The rlog transformation
and VST are offered as separate functionality which can be used for visualization,
clustering or other machine learning tasks. See the transformation section of the
vignette for more details.
</p>
<p>The transformation does not require that one has already estimated size factors
and dispersions.
</p>
<p>A typical workflow is shown in Section <em>Variance stabilizing transformation</em>
in the package vignette.
</p>
<p>If <code><a href="estimateDispersions.html">estimateDispersions</a></code> was called with:
</p>
<p><code>fitType="parametric"</code>,
a closed-form expression for the variance stabilizing
transformation is used on the normalized
count data. The expression can be found in the file &lsquo;<span class="file">vst.pdf</span>&rsquo;
which is distributed with the vignette.
</p>
<p><code>fitType="local"</code>,
the reciprocal of the square root of the variance of the normalized counts, as derived
from the dispersion fit, is then numerically
integrated, and the integral (approximated by a spline function) is evaluated for each
count value in the column, yielding a transformed value. 
</p>
<p><code>fitType="mean"</code>, a VST is applied for Negative Binomial distributed counts, 'k',
with a fixed dispersion, 'a': ( 2 asinh(sqrt(a k)) - log(a) - log(4) )/log(2).
</p>
<p>In all cases, the transformation is scaled such that for large
counts, it becomes asymptotically (for large values) equal to the
logarithm to base 2 of normalized counts.
</p>
<p>The variance stabilizing transformation from a previous dataset
can be frozen and reapplied to new samples. See the 'Data quality assessment'
section of the vignette for strategies to see if new samples are
sufficiently similar to previous datasets. 
The frozen VST is accomplished by saving the dispersion function
accessible with <code><a href="dispersionFunction.html">dispersionFunction</a></code>, assigning this
to the <code>DESeqDataSet</code> with the new samples, and running
varianceStabilizingTransformation with 'blind' set to FALSE
(see example below).
Then the dispersion function from the previous dataset will be used
to transform the new sample(s).
</p>
<p>Limitations: In order to preserve normalization, the same
transformation has to be used for all samples. This results in the
variance stabilizition to be only approximate. The more the size
factors differ, the more residual dependence of the variance on the
mean will be found in the transformed data. <code><a href="rlog.html">rlog</a></code> is a
transformation which can perform better in these cases.
As shown in the vignette, the function <code>meanSdPlot</code>
from the package <span class="pkg">vsn</span> can be used to see whether this is a problem.
</p>


<h3>Value</h3>

<p><code>varianceStabilizingTransformation</code> returns a
<code><a href="DESeqTransform.html">DESeqTransform</a></code> if a <code>DESeqDataSet</code> was provided,
or returns a a matrix if a count matrix was provided.
Note that for <code><a href="DESeqTransform.html">DESeqTransform</a></code> output, the matrix of
transformed values is stored in <code>assay(vsd)</code>.
<code>getVarianceStabilizedData</code> also returns a matrix.
</p>


<h3>Author(s)</h3>

<p>Simon Anders
</p>


<h3>References</h3>

<p>Reference for the variance stabilizing transformation for counts with a dispersion trend:
</p>
<p>Simon Anders, Wolfgang Huber: Differential expression analysis for sequence count data. Genome Biology 2010, 11:106. <a href="http://dx.doi.org/10.1186/gb-2010-11-10-r106">http://dx.doi.org/10.1186/gb-2010-11-10-r106</a>
</p>


<h3>See Also</h3>

<p><code><a href="plotPCA.html">plotPCA</a></code>, <code><a href="rlog.html">rlog</a></code>, <code><a href="normTransform.html">normTransform</a></code>
</p>


<h3>Examples</h3>

<pre>

dds &lt;- makeExampleDESeqDataSet(m=6)
vsd &lt;- varianceStabilizingTransformation(dds)
dists &lt;- dist(t(assay(vsd)))
# plot(hclust(dists))

</pre>

<hr /><div style="text-align: center;">[Package <em>DESeq2</em> version 1.26.0 <a href="00Index.html">Index</a>]</div>
</body></html>
