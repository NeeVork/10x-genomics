<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate the dispersions for a DESeqDataSet</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for estimateDispersions {DESeq2}"><tr><td>estimateDispersions {DESeq2}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Estimate the dispersions for a DESeqDataSet</h2>

<h3>Description</h3>

<p>This function obtains dispersion estimates for Negative Binomial distributed data.
</p>


<h3>Usage</h3>

<pre>
## S4 method for signature 'DESeqDataSet'
estimateDispersions(object,
  fitType = c("parametric", "local", "mean"), maxit = 100,
  quiet = FALSE, modelMatrix = NULL, minmu = 0.5)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>
<p>a DESeqDataSet</p>
</td></tr>
<tr valign="top"><td><code>fitType</code></td>
<td>
<p>either &quot;parametric&quot;, &quot;local&quot;, or &quot;mean&quot;
for the type of fitting of dispersions to the mean intensity.
</p>

<ul>
<li><p> parametric - fit a dispersion-mean relation of the form:
</p>
<p style="text-align: center;"><i>dispersion = asymptDisp + extraPois / mean</i></p>

<p>via a robust gamma-family GLM. The coefficients <code>asymptDisp</code> and <code>extraPois</code>
are given in the attribute <code>coefficients</code> of the <code><a href="dispersionFunction.html">dispersionFunction</a></code>
of the object.
</p>
</li>
<li><p> local - use the locfit package to fit a local regression
of log dispersions over log base mean (normal scale means and dispersions
are input and output for <code><a href="dispersionFunction.html">dispersionFunction</a></code>). The points
are weighted by normalized mean count in the local regression.
</p>
</li>
<li><p> mean - use the mean of gene-wise dispersion estimates.
</p>
</li></ul>
</td></tr>
<tr valign="top"><td><code>maxit</code></td>
<td>
<p>control parameter: maximum number of iterations to allow for convergence</p>
</td></tr>
<tr valign="top"><td><code>quiet</code></td>
<td>
<p>whether to print messages at each step</p>
</td></tr>
<tr valign="top"><td><code>modelMatrix</code></td>
<td>
<p>an optional matrix which will be used for fitting the expected counts.
by default, the model matrix is constructed from <code>design(object)</code></p>
</td></tr>
<tr valign="top"><td><code>minmu</code></td>
<td>
<p>lower bound on the estimated count for fitting gene-wise dispersion</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Typically the function is called with the idiom:
</p>
<p><code>dds &lt;- estimateDispersions(dds)</code>
</p>
<p>The fitting proceeds as follows: for each gene, an estimate of the dispersion
is found which maximizes the Cox Reid-adjusted profile likelihood
(the methods of Cox Reid-adjusted profile likelihood maximization for
estimation of dispersion in RNA-Seq data were developed by McCarthy,
et al. (2012), first implemented in the edgeR package in 2010);
a trend line capturing the dispersion-mean relationship is fit to the maximum likelihood estimates;
a normal prior is determined for the log dispersion estimates centered
on the predicted value from the trended fit
with variance equal to the difference between the observed variance of the
log dispersion estimates and the expected sampling variance;
finally maximum a posteriori dispersion estimates are returned.
This final dispersion parameter is used in subsequent tests.
The final dispersion estimates can be accessed from an object using <code><a href="dispersions.html">dispersions</a></code>.
The fitted dispersion-mean relationship is also used in
<code><a href="varianceStabilizingTransformation.html">varianceStabilizingTransformation</a></code>.
All of the intermediate values (gene-wise dispersion estimates, fitted dispersion
estimates from the trended fit, etc.) are stored in <code>mcols(dds)</code>, with
information about these columns in <code>mcols(mcols(dds))</code>.
</p>
<p>The log normal prior on the dispersion parameter has been proposed
by Wu, et al. (2012) and is also implemented in the DSS package.
</p>
<p>In DESeq2, the dispersion estimation procedure described above replaces the
different methods of dispersion from the previous version of the DESeq package.
</p>
<p><code>estimateDispersions</code> checks for the case of an analysis
with as many samples as the number of coefficients to fit,
and will temporarily substitute a design formula <code>~ 1</code> for the
purposes of dispersion estimation. Note that analysis of designs without
replicates will be removed in the Oct 2018 release: DESeq2 v1.22.0,
after which DESeq2 will give an error.
</p>
<p>The lower-level functions called by <code>estimateDispersions</code> are:
<code><a href="estimateDispersionsGeneEst.html">estimateDispersionsGeneEst</a></code>,
<code><a href="estimateDispersionsGeneEst.html">estimateDispersionsFit</a></code>, and
<code><a href="estimateDispersionsGeneEst.html">estimateDispersionsMAP</a></code>.
</p>


<h3>Value</h3>

<p>The DESeqDataSet passed as parameters, with the dispersion information
filled in as metadata columns, accessible via <code>mcols</code>, or the final dispersions
accessible via <code><a href="dispersions.html">dispersions</a></code>.
</p>


<h3>References</h3>


<ul>
<li><p> Simon Anders, Wolfgang Huber: Differential expression analysis for sequence count data.
Genome Biology 11 (2010) R106, <a href="http://dx.doi.org/10.1186/gb-2010-11-10-r106">http://dx.doi.org/10.1186/gb-2010-11-10-r106</a>
</p>
</li>
<li><p> McCarthy, DJ, Chen, Y, Smyth, GK: Differential expression analysis of multifactor RNA-Seq
experiments with respect to biological variation. Nucleic Acids Research 40 (2012), 4288-4297,
<a href="http://dx.doi.org/10.1093/nar/gks042">http://dx.doi.org/10.1093/nar/gks042</a>
</p>
</li>
<li><p> Wu, H., Wang, C. &amp; Wu, Z. A new shrinkage estimator for dispersion improves differential
expression detection in RNA-seq data. Biostatistics (2012).
<a href="http://dx.doi.org/10.1093/biostatistics/kxs033">http://dx.doi.org/10.1093/biostatistics/kxs033</a>
</p>
</li></ul>



<h3>Examples</h3>

<pre>

dds &lt;- makeExampleDESeqDataSet()
dds &lt;- estimateSizeFactors(dds)
dds &lt;- estimateDispersions(dds)
head(dispersions(dds))

</pre>

<hr /><div style="text-align: center;">[Package <em>DESeq2</em> version 1.26.0 <a href="00Index.html">Index</a>]</div>
</body></html>
