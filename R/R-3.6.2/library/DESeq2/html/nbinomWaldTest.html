<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Wald test for the GLM coefficients</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for nbinomWaldTest {DESeq2}"><tr><td>nbinomWaldTest {DESeq2}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Wald test for the GLM coefficients</h2>

<h3>Description</h3>

<p>This function tests for significance of coefficients in a Negative
Binomial GLM, using previously calculated <code><a href="sizeFactors.html">sizeFactors</a></code>
(or <code><a href="normalizationFactors.html">normalizationFactors</a></code>)
and dispersion estimates.  See <code><a href="DESeq.html">DESeq</a></code> for the GLM formula.
</p>


<h3>Usage</h3>

<pre>
nbinomWaldTest(object, betaPrior = FALSE, betaPriorVar,
  modelMatrix = NULL, modelMatrixType, betaTol = 1e-08, maxit = 100,
  useOptim = TRUE, quiet = FALSE, useT = FALSE, df, useQR = TRUE,
  minmu = 0.5)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>
<p>a DESeqDataSet</p>
</td></tr>
<tr valign="top"><td><code>betaPrior</code></td>
<td>
<p>whether or not to put a zero-mean normal prior on
the non-intercept coefficients</p>
</td></tr>
<tr valign="top"><td><code>betaPriorVar</code></td>
<td>
<p>a vector with length equal to the number of
model terms including the intercept.
betaPriorVar gives the variance of the prior on the sample betas
on the log2 scale. if missing (default) this is estimated from the data</p>
</td></tr>
<tr valign="top"><td><code>modelMatrix</code></td>
<td>
<p>an optional matrix, typically this is set to NULL
and created within the function</p>
</td></tr>
<tr valign="top"><td><code>modelMatrixType</code></td>
<td>
<p>either &quot;standard&quot; or &quot;expanded&quot;, which describe
how the model matrix, X of the formula in <code><a href="DESeq.html">DESeq</a></code>, is
formed. &quot;standard&quot; is as created by <code>model.matrix</code> using the
design formula. &quot;expanded&quot; includes an indicator variable for each
level of factors in addition to an intercept.
betaPrior must be set to TRUE in order for expanded model matrices
to be fit.</p>
</td></tr>
<tr valign="top"><td><code>betaTol</code></td>
<td>
<p>control parameter defining convergence</p>
</td></tr>
<tr valign="top"><td><code>maxit</code></td>
<td>
<p>the maximum number of iterations to allow for convergence of the
coefficient vector</p>
</td></tr>
<tr valign="top"><td><code>useOptim</code></td>
<td>
<p>whether to use the native optim function on rows which do not
converge within maxit</p>
</td></tr>
<tr valign="top"><td><code>quiet</code></td>
<td>
<p>whether to print messages at each step</p>
</td></tr>
<tr valign="top"><td><code>useT</code></td>
<td>
<p>whether to use a t-distribution as a null distribution,
for significance testing of the Wald statistics.
If FALSE, a standard normal null distribution is used.
See next argument <code>df</code> for information about which t is used.
If <code>useT=TRUE</code> then further calls to <code><a href="results.html">results</a></code>
will make use of <code>mcols(object)$tDegreesFreedom</code> that is stored
by <code>nbinomWaldTest</code>.</p>
</td></tr>
<tr valign="top"><td><code>df</code></td>
<td>
<p>the degrees of freedom for the t-distribution.
This can be of length 1 or the number of rows of <code>object</code>.
If this is not specified, the degrees of freedom will be set
by the number of samples minus the number of columns of the design
matrix used for dispersion estimation. If <code>"weights"</code> are included in
the <code>assays(object)</code>, then the sum of the weights is used in lieu
of the number of samples.</p>
</td></tr>
<tr valign="top"><td><code>useQR</code></td>
<td>
<p>whether to use the QR decomposition on the design
matrix X while fitting the GLM</p>
</td></tr>
<tr valign="top"><td><code>minmu</code></td>
<td>
<p>lower bound on the estimated count while fitting the GLM</p>
</td></tr>
</table>


<h3>Details</h3>

<p>The fitting proceeds as follows: standard maximum likelihood estimates
for GLM coefficients (synonymous with &quot;beta&quot;, &quot;log2 fold change&quot;, &quot;effect size&quot;)
are calculated.
Then, optionally, a zero-centered Normal prior distribution 
(<code>betaPrior</code>) is assumed for the coefficients other than the intercept.
</p>
<p>Note that this posterior log2 fold change
estimation is now not the default setting for <code>nbinomWaldTest</code>,
as the standard workflow for coefficient shrinkage has moved to
an additional function <code>link{lfcShrink}</code>.
</p>
<p>For calculating Wald test p-values, the coefficients are scaled by their
standard errors and then compared to a standard Normal distribution. 
The <code><a href="results.html">results</a></code>
function without any arguments will automatically perform a contrast of the
last level of the last variable in the design formula over the first level.
The <code>contrast</code> argument of the <code><a href="results.html">results</a></code> function can be used
to generate other comparisons.
</p>
<p>The Wald test can be replaced with the <code><a href="nbinomLRT.html">nbinomLRT</a></code>
for an alternative test of significance.
</p>
<p>Notes on the log2 fold change prior:
</p>
<p>The variance of the prior distribution for each
non-intercept coefficient is calculated using the observed
distribution of the maximum likelihood coefficients.  
The final coefficients are then maximum a posteriori estimates
using this prior (Tikhonov/ridge regularization). 
See below for details on the
prior variance and the Methods section of the DESeq2 manuscript for more detail.
The use of a prior has little effect on genes with high counts and helps to
moderate the large spread in coefficients for genes with low counts.
</p>
<p>The prior variance is calculated by matching the 0.05 upper quantile
of the observed MLE coefficients to a zero-centered Normal distribution.
In a change of methods since the 2014 paper,
the weighted upper quantile is calculated using the
<code>wtd.quantile</code> function from the Hmisc package. The weights are
the inverse of the expected variance of log counts, so the inverse of
<i>1/mu-bar + alpha_tr</i> using the mean of
normalized counts and the trended dispersion fit. The weighting ensures
that noisy estimates of log fold changes from small count genes do not
overly influence the calculation of the prior variance.
See <code><a href="estimateBetaPriorVar.html">estimateBetaPriorVar</a></code>.
The final prior variance for a factor level is the average of the
estimated prior variance over all contrasts of all levels of the factor.
</p>
<p>When a log2 fold change prior is used (betaPrior=TRUE),
then <code>nbinomWaldTest</code> will by default use expanded model matrices,
as described in the <code>modelMatrixType</code> argument, unless this argument
is used to override the default behavior.
This ensures that log2 fold changes will be independent of the choice
of reference level. In this case, the beta prior variance for each factor
is calculated as the average of the mean squared maximum likelihood
estimates for each level and every possible contrast.
</p>


<h3>Value</h3>

<p>a DESeqDataSet with results columns accessible
with the <code><a href="results.html">results</a></code> function.  The coefficients and standard errors are
reported on a log2 scale.
</p>


<h3>See Also</h3>

<p><code><a href="DESeq.html">DESeq</a></code>, <code><a href="nbinomLRT.html">nbinomLRT</a></code>
</p>


<h3>Examples</h3>

<pre>

dds &lt;- makeExampleDESeqDataSet()
dds &lt;- estimateSizeFactors(dds)
dds &lt;- estimateDispersions(dds)
dds &lt;- nbinomWaldTest(dds)
res &lt;- results(dds)

</pre>

<hr /><div style="text-align: center;">[Package <em>DESeq2</em> version 1.26.0 <a href="00Index.html">Index</a>]</div>
</body></html>
