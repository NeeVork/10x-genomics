<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Estimate the size factors for a 'DESeqDataSet'</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for estimateSizeFactors {DESeq2}"><tr><td>estimateSizeFactors {DESeq2}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Estimate the size factors for a <code><a href="DESeqDataSet.html">DESeqDataSet</a></code></h2>

<h3>Description</h3>

<p>This function estimates the size factors using the
&quot;median ratio method&quot; described by Equation 5 in Anders and Huber (2010).
The estimated size factors can be accessed using the accessor function <code><a href="sizeFactors.html">sizeFactors</a></code>.
Alternative library size estimators can also be supplied
using the assignment function <code><a href="../../BiocGenerics/html/dge.html">sizeFactors&lt;-</a></code>.
</p>


<h3>Usage</h3>

<pre>
## S4 method for signature 'DESeqDataSet'
estimateSizeFactors(object, type = c("ratio",
  "poscounts", "iterate"), locfunc = stats::median, geoMeans,
  controlGenes, normMatrix, quiet = FALSE)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>
<p>a DESeqDataSet</p>
</td></tr>
<tr valign="top"><td><code>type</code></td>
<td>
<p>Method for estimation: either &quot;ratio&quot;, &quot;poscounts&quot;, or &quot;iterate&quot;.
&quot;ratio&quot; uses the standard median ratio method introduced in DESeq. The size factor is the
median ratio of the sample over a &quot;pseudosample&quot;: for each gene, the geometric mean
of all samples.
&quot;poscounts&quot; and &quot;iterate&quot; offer alternative estimators, which can be
used even when all genes contain a sample with a zero (a problem for the
default method, as the geometric mean becomes zero, and the ratio undefined).
The &quot;poscounts&quot; estimator deals with a gene with some zeros, by calculating a
modified geometric mean by taking the n-th root of the product of the non-zero counts.
This evolved out of use cases with Paul McMurdie's phyloseq package for metagenomic samples.
The &quot;iterate&quot; estimator iterates between estimating the dispersion with a design of ~1, and
finding a size factor vector by numerically optimizing the likelihood
of the ~1 model.</p>
</td></tr>
<tr valign="top"><td><code>locfunc</code></td>
<td>
<p>a function to compute a location for a sample. By default, the
median is used. However, especially for low counts, the
<code><a href="../../genefilter/html/shorth.html">shorth</a></code> function from the genefilter package may give better results.</p>
</td></tr>
<tr valign="top"><td><code>geoMeans</code></td>
<td>
<p>by default this is not provided and the
geometric means of the counts are calculated within the function.
A vector of geometric means from another count matrix can be provided
for a &quot;frozen&quot; size factor calculation</p>
</td></tr>
<tr valign="top"><td><code>controlGenes</code></td>
<td>
<p>optional, numeric or logical index vector specifying those genes to
use for size factor estimation (e.g. housekeeping or spike-in genes)</p>
</td></tr>
<tr valign="top"><td><code>normMatrix</code></td>
<td>
<p>optional, a matrix of normalization factors which do not yet
control for library size. Note that this argument should not be used (and
will be ignored) if the <code>dds</code> object was created using <code>tximport</code>.
In this case, the information in <code>assays(dds)[["avgTxLength"]]</code>
is automatically used to create appropriate normalization factors.
Providing <code>normMatrix</code> will estimate size factors on the
count matrix divided by <code>normMatrix</code> and store the product of the
size factors and <code>normMatrix</code> as <code><a href="normalizationFactors.html">normalizationFactors</a></code>.
It is recommended to divide out the row-wise geometric mean of
<code>normMatrix</code> so the rows roughly are centered on 1.</p>
</td></tr>
<tr valign="top"><td><code>quiet</code></td>
<td>
<p>whether to print messages</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Typically, the function is called with the idiom:
</p>
<p><code>dds &lt;- estimateSizeFactors(dds)</code>
</p>
<p>See <code><a href="DESeq.html">DESeq</a></code> for a description of the use of size factors in the GLM.
One should call this function after <code><a href="DESeqDataSet.html">DESeqDataSet</a></code>
unless size factors are manually specified with <code><a href="sizeFactors.html">sizeFactors</a></code>.
Alternatively, gene-specific normalization factors for each sample can be provided using
<code><a href="normalizationFactors.html">normalizationFactors</a></code> which will always preempt <code><a href="sizeFactors.html">sizeFactors</a></code>
in calculations.
</p>
<p>Internally, the function calls <code><a href="estimateSizeFactorsForMatrix.html">estimateSizeFactorsForMatrix</a></code>, 
which provides more details on the calculation.
</p>


<h3>Value</h3>

<p>The DESeqDataSet passed as parameters, with the size factors filled
in.
</p>


<h3>Author(s)</h3>

<p>Simon Anders
</p>


<h3>References</h3>

<p>Reference for the median ratio method:
</p>
<p>Simon Anders, Wolfgang Huber: Differential expression analysis for sequence count data.
Genome Biology 2010, 11:106. <a href="http://dx.doi.org/10.1186/gb-2010-11-10-r106">http://dx.doi.org/10.1186/gb-2010-11-10-r106</a>
</p>


<h3>See Also</h3>

<p><code><a href="estimateSizeFactorsForMatrix.html">estimateSizeFactorsForMatrix</a></code>
</p>


<h3>Examples</h3>

<pre>

dds &lt;- makeExampleDESeqDataSet(n=1000, m=4)
dds &lt;- estimateSizeFactors(dds)
sizeFactors(dds)

dds &lt;- estimateSizeFactors(dds, controlGenes=1:200)

m &lt;- matrix(runif(1000 * 4, .5, 1.5), ncol=4)
dds &lt;- estimateSizeFactors(dds, normMatrix=m)
normalizationFactors(dds)[1:3,]

geoMeans &lt;- exp(rowMeans(log(counts(dds))))
dds &lt;- estimateSizeFactors(dds,geoMeans=geoMeans)
sizeFactors(dds)

</pre>

<hr /><div style="text-align: center;">[Package <em>DESeq2</em> version 1.26.0 <a href="00Index.html">Index</a>]</div>
</body></html>
