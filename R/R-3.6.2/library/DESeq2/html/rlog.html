<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Apply a 'regularized log' transformation</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for rlog {DESeq2}"><tr><td>rlog {DESeq2}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Apply a 'regularized log' transformation</h2>

<h3>Description</h3>

<p>This function transforms the count data to the log2 scale in a way 
which minimizes differences between samples for rows with small counts,
and which normalizes with respect to library size.
The rlog transformation produces a similar variance stabilizing effect as
<code><a href="varianceStabilizingTransformation.html">varianceStabilizingTransformation</a></code>,
though <code>rlog</code> is more robust in the
case when the size factors vary widely.
The transformation is useful when checking for outliers
or as input for machine learning techniques
such as clustering or linear discriminant analysis.
<code>rlog</code> takes as input a <code><a href="DESeqDataSet.html">DESeqDataSet</a></code> and returns a
<code><a href="../../SummarizedExperiment/html/RangedSummarizedExperiment-class.html">RangedSummarizedExperiment</a></code> object.
</p>


<h3>Usage</h3>

<pre>
rlog(object, blind = TRUE, intercept, betaPriorVar,
  fitType = "parametric")

rlogTransformation(object, blind = TRUE, intercept, betaPriorVar,
  fitType = "parametric")
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>object</code></td>
<td>
<p>a DESeqDataSet, or matrix of counts</p>
</td></tr>
<tr valign="top"><td><code>blind</code></td>
<td>
<p>logical, whether to blind the transformation to the experimental
design. blind=TRUE should be used for comparing samples in an manner unbiased by
prior information on samples, for example to perform sample QA (quality assurance).
blind=FALSE should be used for transforming data for downstream analysis,
where the full use of the design information should be made.
blind=FALSE will skip re-estimation of the dispersion trend, if this has already been calculated.
If many of genes have large differences in counts due to
the experimental design, it is important to set blind=FALSE for downstream
analysis.</p>
</td></tr>
<tr valign="top"><td><code>intercept</code></td>
<td>
<p>by default, this is not provided and calculated automatically.
if provided, this should be a vector as long as the number of rows of object,
which is log2 of the mean normalized counts from a previous dataset.
this will enforce the intercept for the GLM, allowing for a &quot;frozen&quot; rlog
transformation based on a previous dataset.
You will also need to provide <code>mcols(object)$dispFit</code>.</p>
</td></tr>
<tr valign="top"><td><code>betaPriorVar</code></td>
<td>
<p>a single value, the variance of the prior on the sample
betas, which if missing is estimated from the data</p>
</td></tr>
<tr valign="top"><td><code>fitType</code></td>
<td>
<p>in case dispersions have not yet been estimated for <code>object</code>,
this parameter is passed on to <code><a href="estimateDispersions.html">estimateDispersions</a></code> (options described there).</p>
</td></tr>
</table>


<h3>Details</h3>

<p>Note that neither rlog transformation nor the VST are used by the
differential expression estimation in <code><a href="DESeq.html">DESeq</a></code>, which always
occurs on the raw count data, through generalized linear modeling which
incorporates knowledge of the variance-mean dependence. The rlog transformation
and VST are offered as separate functionality which can be used for visualization,
clustering or other machine learning tasks. See the transformation section of the
vignette for more details, including a statement on timing. If <code>rlog</code>
is run on data with number of samples in [30-49] it will print a message
that it may take a few minutes, if the number of samples is 50 or larger, it
will print a message that it may take a &quot;long time&quot;, and in both cases, it
will mention that the <code><a href="vst.html">vst</a></code> is a much faster transformation.
</p>
<p>The transformation does not require that one has already estimated size factors
and dispersions.
</p>
<p>The regularization is on the log fold changes of the count for each sample
over an intercept, for each gene. As nearby count values for low counts genes
are almost as likely as the observed count, the rlog shrinkage is greater for low counts.
For high counts, the rlog shrinkage has a much weaker effect.
The fitted dispersions are used rather than the MAP dispersions
(so similar to the <code><a href="varianceStabilizingTransformation.html">varianceStabilizingTransformation</a></code>).
</p>
<p>The prior variance for the shrinkag of log fold changes is calculated as follows: 
a matrix is constructed of the logarithm of the counts plus a pseudocount of 0.5,
the log of the row means is then subtracted, leaving an estimate of
the log fold changes per sample over the fitted value using only an intercept.
The prior variance is then calculated by matching the upper quantiles of the observed 
log fold change estimates with an upper quantile of the normal distribution.
A GLM fit is then calculated using this prior. It is also possible to supply the variance of the prior.
See the vignette for an example of the use and a comparison with <code>varianceStabilizingTransformation</code>.
</p>
<p>The transformed values, rlog(K), are equal to
<i>rlog(K_ij) = log2(q_ij) = beta_i0 + beta_ij</i>,
with formula terms defined in <code><a href="DESeq.html">DESeq</a></code>.
</p>
<p>The parameters of the rlog transformation from a previous dataset
can be frozen and reapplied to new samples. See the 'Data quality assessment'
section of the vignette for strategies to see if new samples are
sufficiently similar to previous datasets. 
The frozen rlog is accomplished by saving the dispersion function,
beta prior variance and the intercept from a previous dataset,
and running <code>rlog</code> with 'blind' set to FALSE
(see example below).
</p>


<h3>Value</h3>

<p>a <code><a href="DESeqTransform.html">DESeqTransform</a></code> if a <code>DESeqDataSet</code> was provided,
or a matrix if a count matrix was provided as input.
Note that for <code><a href="DESeqTransform.html">DESeqTransform</a></code> output, the matrix of
transformed values is stored in <code>assay(rld)</code>.
To avoid returning matrices with NA values, in the case of a row
of all zeros, the rlog transformation returns zeros
(essentially adding a pseudocount of 1 only to these rows).
</p>


<h3>References</h3>

<p>Reference for regularized logarithm (rlog):
</p>
<p>Michael I Love, Wolfgang Huber, Simon Anders: Moderated estimation of fold change and dispersion for RNA-seq data with DESeq2. Genome Biology 2014, 15:550. <a href="http://dx.doi.org/10.1186/s13059-014-0550-8">http://dx.doi.org/10.1186/s13059-014-0550-8</a>
</p>


<h3>See Also</h3>

<p><code><a href="plotPCA.html">plotPCA</a></code>, <code><a href="varianceStabilizingTransformation.html">varianceStabilizingTransformation</a></code>, <code><a href="normTransform.html">normTransform</a></code>
</p>


<h3>Examples</h3>

<pre>

dds &lt;- makeExampleDESeqDataSet(m=6,betaSD=1)
rld &lt;- rlog(dds)
dists &lt;- dist(t(assay(rld)))
# plot(hclust(dists))

</pre>

<hr /><div style="text-align: center;">[Package <em>DESeq2</em> version 1.26.0 <a href="00Index.html">Index</a>]</div>
</body></html>
