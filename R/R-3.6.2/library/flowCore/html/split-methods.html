<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Methods to split flowFrames and flowSets according to filters</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for split-methods {flowCore}"><tr><td>split-methods {flowCore}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Methods to split flowFrames and flowSets according to filters</h2>

<h3>Description</h3>

<p>Divide a flow cytometry data set into several subset according to the
results of a filtering operation. There are also methods available to split
according to a factor variable.
</p>


<h3>Details</h3>

<p>The splitting operation in the context of <code><a href="flowFrame-class.html">flowFrame</a></code>s
and <code><a href="flowSet-class.html">flowSet</a></code>s is the logical extension of subsetting.
While the latter only returns the events contained within a gate, the former
splits the data into the groups of events contained within and those not
contained within a particular gate. This concept is extremely useful in
applications where gates describe the distinction between positivity and
negativity for a particular marker.
</p>
<p>The flow data structures in <code>flowCore</code> can be split into subsets on
various levels:
</p>
<p><code><a href="flowFrame-class.html">flowFrame</a></code>: row-wise splitting of the raw data. In most
cases, this will be done according to the outcome of a filtering operation,
either using a filter that identifiers more than one sub-population or by a
logical filter, in which case the data is split into two populations: &quot;in
the filter&quot; and &quot;not in the filter&quot;. In addition, the data can be split
according to a factor (or a numeric or character vector that can be coerced
into a factor).
</p>
<p><code><a href="flowSet-class.html">flowSet</a></code>: can be either split into subsets of
<code><a href="flowFrame-class.html">flowFrame</a></code>s according to a factor or a vector that can
be coerced into a factor, or each individual <code><a href="flowFrame-class.html">flowFrame</a></code>
into subpopulations based on the <code><a href="filter-class.html">filter</a></code>s or
<code><a href="filterResult-class.html">filterResult</a></code>s provided as a list of equal length.
</p>
<p>Splitting has a special meaning for filters that result in
<code><a href="multipleFilterResult-class.html">multipleFilterResult</a></code>s or
<code><a href="manyFilterResult-class.html">manyFilterResult</a></code>s, in which case simple subsetting
doesn't make much sense (there are multiple populations that are defined by
the gate and it is not clear which of those should be used for the
subsetting operation). Accordingly, splitting of multipleFilterResults
creates multiple subsets. The argument <code>population</code> can be used to
limit the output to only one or some of the resulting subsets. It takes as
values a character vector of names of the populations of interest. See the
documentation of the different filter classes on how population names can be
defined and the respective default values. For splitting of
<code><a href="logicalFilterResult-class.html">logicalFilterResult</a></code>s, the <code>population</code> argument
can be used to set the population names since there is no reasonable default
other than the name of the gate. The content of the argument <code>prefix</code>
will be prepended to the population names and '+' or '-' are finally
appended allowing for more flexible naming schemes.
</p>
<p>The default return value for any of the <code>split</code> methods is a list, but
the optional logical argument <code>flowSet</code> can be used to return a
<code><a href="flowSet-class.html">flowSet</a></code> instead. This only applies when splitting
<code><a href="flowFrame-class.html">flowFrame</a></code>s, splitting of <code><a href="flowSet-class.html">flowSet</a></code>s
always results in lists of <code><a href="flowSet-class.html">flowSet</a></code> objects.
</p>


<h3>Methods</h3>

<p><code><a href="flowFrame-class.html">flowFrame</a></code> methods:
</p>

<dl>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;ANY&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Catch all input and cast an
error if there is no method for <code>f</code> to dispatch to. </p>
</dd>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;factor&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Split a
<code><a href="flowFrame-class.html">flowFrame</a></code> by a factor variable. Length of <code>f</code> should be
the same as <code>nrow(x)</code>, otherwise it will be recycled, possibly leading
to undesired outcomes. The optional argument <code>drop</code> works in the usual
way, in that it removes empty levels from the factor before splitting.</p>
</dd>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;character&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Coerce <code>f</code> to a
factor and split on that. </p>
</dd>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;numeric&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Coerce <code>f</code> to a
factor and split on that. </p>
</dd>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;filter&quot;, drop = &quot;ANY&quot;)</dt><dd><p> First applies the
<code><a href="filter-class.html">filter</a></code> to the <code><a href="flowFrame-class.html">flowFrame</a></code> and then
splits on the resulting <code><a href="filterResult-class.html">filterResult</a></code> object. </p>
</dd>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;logicalFilterResult&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Split into
the two subpopulations (in and out of the gate). The optional argument
<code>population</code> can be used to control the names of the results. </p>
</dd>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;manyFilterResult&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Split into the
several subpopulations identified by the filtering operation. Instead of
returning a list, the additional logical argument codeflowSet makes the
method return an object of class <code><a href="flowSet-class.html">flowSet</a></code>. The optional
<code>population</code> argument takes a character vector indicating the
subpopulations to use for splitting (as identified by the population name in
the <code>filterDetails</code> slot).</p>
</dd>
<dt>split(x = &quot;flowFrame&quot;, f = &quot;multipleFilterResult&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Split into
the several subpopulations identified by the filtering operation. Instead of
returning a list, the additional logical argument codeflowSet makes the
method return an object of class <code><a href="flowSet-class.html">flowSet</a></code>. The optional
<code>population</code> argument takes a character vector indicating the
subpopulations to use for splitting (as identified by the population name in
the <code>filterDetails</code> slot). Alternatively, this can be a list of
characters, in which case the populations for each list item are collapsed
into one <code><a href="flowFrame-class.html">flowFrame</a></code>.</p>
</dd>
</dl>

<p><code><a href="flowSet-class.html">flowSet</a></code> methods:
</p>

<dl>
<dt>split(x = &quot;flowSet&quot;, f = &quot;ANY&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Catch all input and cast an
error if there is no method for <code>f</code> to dispatch to.  </p>
</dd>
<dt>split(x = &quot;flowSet&quot;, f = &quot;factor&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Split a
<code><a href="flowSet-class.html">flowSet</a></code> by a factor variable. Length of <code>f</code> needs to be
the same as <code>length(x)</code>. The optional argument <code>drop</code> works in the
usual way, in that it removes empty levels from the factor before splitting.
</p>
</dd>
<dt>split(x = &quot;flowSet&quot;, f = &quot;character&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Coerce <code>f</code> to a
factor and split on that. </p>
</dd>
<dt>split(x = &quot;flowSet&quot;, f = &quot;numeric&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Coerce <code>f</code> to a
factor and split on that. </p>
</dd>
<dt>split(x = &quot;flowSet&quot;, f = &quot;list&quot;, drop = &quot;ANY&quot;)</dt><dd><p> Split a
<code><a href="flowSet-class.html">flowSet</a></code> by a list of <code><a href="filterResult-class.html">filterResult</a></code>s (as
typically returned by filtering operations on a
<code><a href="flowSet-class.html">flowSet</a></code>). The length of the list has to be equal to the
length of the <code><a href="flowSet-class.html">flowSet</a></code> and every list item needs to be a
<code><a href="filterResult-class.html">filterResult</a></code> of equal class with the same parameters.
Instead of returning a list, the additional logical argument codeflowSet
makes the method return an object of class <code><a href="flowSet-class.html">flowSet</a></code>. The
optional <code>population</code> argument takes a character vector indicating the
subpopulations to use for splitting (as identified by the population name in
the <code>filterDetails</code> slot).  Alternatively, this can be a list of
characters, in which case the populations for each list item are collapsed
into one <code><a href="flowFrame-class.html">flowFrame</a></code>. Note that using the
<code>population</code> argument implies common population names for
all<code><a href="filterResult-class.html">filterResult</a></code>s in the list and there will be an error
if this is not the case. </p>
</dd>
</dl>



<h3>Author(s)</h3>

<p>F Hahne, B. Ellis, N. Le Meur
</p>


<h3>Examples</h3>

<pre>

data(GvHD)
qGate &lt;- quadGate(filterId="qg", "FSC-H"=200, "SSC-H"=400)

## split a flowFrame by a filter that creates
## a multipleFilterResult
samp &lt;- GvHD[[1]]
fres &lt;- filter(samp, qGate)
split(samp, qGate)

## return a flowSet rather than a list
split(samp, fres, flowSet=TRUE)

## only keep one population
names(fres)
##split(samp, fres, population="FSC-Height+SSC-Height+")


## split the whole set, only keep two populations
##split(GvHD, qGate, population=c("FSC-Height+SSC-Height+",
##"FSC-Height-SSC-Height+"))

## now split the flowSet according to a factor
split(GvHD, pData(GvHD)$Patient)

</pre>

<hr /><div style="text-align: center;">[Package <em>flowCore</em> version 1.52.1 <a href="00Index.html">Index</a>]</div>
</body></html>
