<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><title>R: Simplified geometric transformation of gates</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="R.css" />
</head><body>

<table width="100%" summary="page for transform_gate {flowCore}"><tr><td>transform_gate {flowCore}</td><td style="text-align: right;">R Documentation</td></tr></table>

<h2>Simplified geometric transformation of gates</h2>

<h3>Description</h3>

<p>Perform geometric transformations of Gate-type <code><a href="filter-class.html">filter</a></code> objects
</p>


<h3>Usage</h3>

<pre>
## Default S3 method:
transform_gate(obj, scale = NULL, deg = NULL,
  rot_center = NULL, dx = NULL, dy = NULL, center = NULL, ...)
</pre>


<h3>Arguments</h3>

<table summary="R argblock">
<tr valign="top"><td><code>obj</code></td>
<td>
<p>A Gate-type <code><a href="filter-methods.html">filter</a></code> object (<code><a href="quadGate-class.html">quadGate</a></code>,
<code><a href="rectangleGate-class.html">rectangleGate</a></code>, <code><a href="ellipsoidGate-class.html">ellipsoidGate</a></code>, or <code><a href="polygonGate-class.html">polygonGate</a></code>)</p>
</td></tr>
<tr valign="top"><td><code>scale</code></td>
<td>
<p>Either a numeric scalar (for uniform scaling in all dimensions) or numeric vector specifying the factor by 
which each dimension of the gate should be expanded (absolute value &gt; 1) or contracted (absolute value &lt; 1). Negative values 
will result in a reflection in that dimension. 
</p>
<p>For <code>rectangleGate</code> and <code>quadGate</code> objects, this amounts to simply
scaling the values of the 1-dimensional boundaries. For <code>polygonGate</code> objects, the values of <code>scale</code> will be used
to determine scale factors in the direction of each of the 2 dimensions of the gate (<code>scale_gate</code> is not yet defined
for higher-dimensional <code>polytopeGate</code> objects). <strong>Important: </strong> For <code>ellipsoidGate</code> objects, <code>scale</code>
determines scale factors for the major and minor axes of the ellipse, in that order.</p>
</td></tr>
<tr valign="top"><td><code>deg</code></td>
<td>
<p>An angle in degrees by which the gate should be rotated in the counter-clockwise direction.</p>
</td></tr>
<tr valign="top"><td><code>rot_center</code></td>
<td>
<p>A separate 2-dimensional center of rotation for the gate, if desired. By default, this will
be the center for <code>ellipsoidGate</code> objects or the centroid for <code>polygonGate</code> objects. The <code>rot_center</code> argument 
is currently only supported for <code>polygonGate</code> objects. It is also usually simpler to perform a rotation and a translation 
individually than to manually specify the composition as a rotation around a shifted center.</p>
</td></tr>
<tr valign="top"><td><code>dx</code></td>
<td>
<p>Either a numeric scalar or numeric vector. If it is scalar, this is just the desired shift of the gate in 
its first dimension. If it is a vector, it specifies both <code>dx</code> and <code>dy</code> as <code>(dx,dy)</code>.
This provides an alternate syntax for shifting gates, as well as allowing shifts of <code>ellipsoidGate</code> objects
in more than 2 dimensions.</p>
</td></tr>
<tr valign="top"><td><code>dy</code></td>
<td>
<p>A numeric scalar specifying the desired shift of the gate in its second dimension.</p>
</td></tr>
<tr valign="top"><td><code>center</code></td>
<td>
<p>A numeric vector specifying where the center or centroid should be moved (rather than specifiying <code>dx</code> 
and/or <code>dy</code>)</p>
</td></tr>
<tr valign="top"><td><code>...</code></td>
<td>
<p>Assignments made to the slots of the particular Gate-type filter object in the form &quot;&lt;slot_name&gt; = &lt;value&gt;&quot;</p>
</td></tr>
</table>


<h3>Details</h3>

<p>This method allows changes to the four filter types defined by simple geometric gates (<code><a href="quadGate-class.html">quadGate</a></code>,
<code><a href="rectangleGate-class.html">rectangleGate</a></code>, <code><a href="ellipsoidGate-class.html">ellipsoidGate</a></code>, and <code><a href="polygonGate-class.html">polygonGate</a></code>) using
equally simple geometric transformations (shifting/translation, scaling/dilation, and rotation). The method also
allows for directly re-setting the slots of each Gate-type object. Note that these methods are for manually altering
the geometric definition of a gate. To easily transform the definition of a gate with an accompanyging scale 
transformation applied to its underlying data, see <code><a href="../../ggcyto/html/rescale_gate.html">rescale_gate</a></code>.
</p>
<p>First, <code>transform_gate</code> will apply any direct alterations to the slots of the supplied Gate-type filter object.
For example, if &quot;<code>mean = c(1,3)</code>&quot; is present in the argument list when <code>transform_gate</code> is called on a
<code>ellipsoidGate</code> object, the first change applied will be to shift the <code>mean</code> slot to <code>(1,3)</code>. The method
will carry over the dimension names from the gate, so there is no need to provide column or row names with arguments
such as <code>mean</code> or <code>cov</code> for <code>ellipsoidGate</code> or <code>boundaries</code> for <code>polygonGate</code>.
</p>
<p><code>transform_gate</code> then passes the geometric arguments (<code>dx</code>, <code>dy</code>, <code>deg</code>, <code>rot_center</code>, <code>scale</code>, 
and <code>center</code>) to the methods which perform each respective type of transformation:  
<code><a href="shift_gate.html">shift_gate</a></code>, <code><a href="scale_gate.html">scale_gate</a></code>, or <code><a href="rotate_gate.html">rotate_gate</a></code>. The order of operations is to first
scale, then rotate, then shift. The default behavior of each operation follows that of its corresponding method but for
the most part these are what the user would expect. A few quick notes:
</p>

<ul>
<li> <p><code>rotate_gate</code> is not defined for <code>rectangleGate</code> or <code>quadGate</code> objects, due to their definition as
having 1-dimensional boundaries.
</p>
</li>
<li><p> The default center for both rotation and scaling of a <code>polygonGate</code> is the centroid of the polygon. This
results in the sort of scaling most users expect, with a uniform scale factor not distorting the shape of the original polygon.
</p>
</li></ul>



<h3>Value</h3>

<p>A Gate-type <code>filter</code> object of the same type as <code>gate</code>, with the geometric transformations applied
</p>


<h3>Examples</h3>

<pre>
## Not run: 
# Scale the original gate non-uniformly, rotate it 15 degrees, and shift it
transformed_gate &lt;- transform_gate(original_gate, scale = c(2,3), deg = 15, dx = 500, dy = -700)

# Scale the original gate (in this case an ellipsoidGate) after moving its center to (1500, 2000)
transformed_gate &lt;- transform_gate(original_gate, scale = c(2,3), mean = c(1500, 2000))

## End(Not run)

</pre>

<hr /><div style="text-align: center;">[Package <em>flowCore</em> version 1.52.1 <a href="00Index.html">Index</a>]</div>
</body></html>
